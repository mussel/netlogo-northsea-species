<!--
SPDX-FileContributor:Carsten Lemmen <carsten.lemmen@hereon.de>
SPDX-FileCopyrightText: 2024 Helmholtz-Zentrum hereon GmbH
SPDX-License-Identifier: CC0-1.0
-->

This directory contains the final publication in the Journal of Open Software (JOSS)

[![status](https://joss.theoj.org/papers/84a737c77c6d676d0aefbcef8974b138/status.svg)](https://doi.org/10.21105/joss.05731)

Lemmen et al., (2024). Viable North Sea (ViNoS): A NetLogo Agent-based Model of German Small-scale Fisheries. Journal of Open Source Software, 9(95), 5731, https://doi.org/10.21105/joss.05731

The sources used to build this manuscript are contained in the `.src` subfolder.
