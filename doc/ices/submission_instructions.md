<!--
SPDX-FileCopyrightText: 2024 Oxford University Press
SPDX-License-Identifier: CC0-1.0
-->

# Categories of Papers

The ICES Journal of Marine Science publishes papers in each of the following categories:

Original articles: traditional research papers. Generally 5-12 printed pages (approximately 3000- 7000 words) inclusive of text, appropriate references (target 50-60), figures, and tables.

Comment/Reply articles: comments on, or replies to, papers published in the ICES Journal of Marine Science; Generally 4 printed pages (approximately 2000 words).

Review articles: comprehensive and synthetic reviews on topics of wide general interest or on hot topics, sometimes solicited by the editorial board. Authors considering submitting a review article for consideration are advised to contact the Editor-in-Chief regarding topic and proposed length.

Food for Thought articles: articles on topical issues, preferably provocative or innovative in nature, solicited or offered to stimulate dialogue, research, and ideas generally.

Quo Vadimus articles: describing the future landscape/potential of a topic, issue, discipline or technology.

Stories from the front lines: substantive accounts of challenges, wins and losses on any aspect of ocean and coastal sustainability, written as narratives and drawing at least partly on the author(s) experience.

# How to get published in ICESJMS

ICES Journal of Marine Science (henceforth, the Journal) is the flagship publication of the International Council for the Exploration of the Sea. The Journal seeks to (i) efficiently and promptly publish rigorous, accessible, and entertaining material that will help marine scientists in their daily work, lifelong learning, and career development, (ii) in so doing, strive to publish only articles that are signals in an ever-increasing sea of noise, (iii) be at the forefront of the international debate on all aspects of marine science, (iv) be among the world's most influential and widely read fisheries and marine science journals.

The Journal strives to serve the fisheries and marine science community by publishing the highest quality research articles that contribute significantly to our understanding of marine ecosystems and the impact of human activities on them. We welcome work from anywhere in the world and encourage submissions from early career scientists as well as from leading researchers in the field.

By paying careful attention to the advice below, authors can help the Journal to achieve these aims and increase their chances of manuscript acceptance.

## Points to consider when deciding whether to submit your manuscript

Approximately 50% of submissions are declined after editorial pre-screening. The overall acceptance rate for original articles is approximately 30%.

The most common reason for immediate rejection is that the material reported upon in the manuscript does not fall within the scope of the Journal. Therefore, read the Journal’s scope and mission statements carefully and make sure that your research is a good match. Look through recent issues of the Journal to ensure that you find articles that are related to the type of research that you report in your manuscript. Be sure that there is a member of the Journal’s editorial board who has the expertise to handle the manuscript.

The following broadly characterized manuscript types are unlikely to be pursued through peer review:

Principally descriptive studies that are absent of a clear objective or hypothesis, or which are narrow in scientific scope or relevance.
Studies that are based upon limited data sets, for example, low level of replication; small sample size; limited number of sample dates/locations; short time series etc.
Species-specific or regional studies that may be of local importance but are not set in a wider context nor integrated with analogous work conducted elsewhere.
Case studies that are confirmatory of a large body of earlier work and that do not clearly add something novel that extends our understanding of the question.
The Journal will typically decline to publish articles that are repurposed from internal, institutional, or governmental reports unless they have been recast as strong stand-alone research articles and their content is clearly and substantively different from the previously published documents.

If you are unsure about any of the above, contact the Editor-in-Chief or a member of the editorial board with a pre-submission inquiry.

## Points to consider when preparing your manuscript

Authors should provide a cover letter with their submission that summarizes the significance and importance of the research. The cover letter forms an important part of the initial evaluation of a manuscript.

Consider carefully which is the most appropriate article type for your manuscript and follow the format instructions on the Journal’s website carefully when you prepare it.

An accurate and informative title, abstract, and key words are important. For online bibliographic searching these have a key role in drawing the attention of potential readers to your paper.

Excessive length and/or inappropriately high number of Figures and Tables will result in the manuscript being returned without review. The Journal provides the opportunity to include Supplementary Material online and also actively encourages data archiving.

Clearly and concisely stating the aim of the work at the beginning of a manuscript, and then coming back in a conclusion or summary to state the outcomes and significance, helps both the reviewers, and ultimately the readers, of your work.

Unclear writing and poor structure impede the review process, sometimes resulting in a negative recommendation. To avoid reviewers dwelling on stylistic details at the expense of scientific content, authors are advised to submit only polished manuscripts. The Journal recognises the challenges of non-English speaking authors; manuscripts will only be rejected on the basis of language if initial review is not possible due to an inability to understand the content. However, a standard of English appropriate for an international journal will make the work of the reviewers easier and will increase the chance of a more favourable reception.

## Common reasons for rejection of manuscripts following peer review

Approximately 40% of the manuscripts submitted for peer review are ultimately rejected. The most common reasons are: flawed study design; inappropriate methodology or statistical analysis; a lack of detail/clarity in the methods that prevent reviewers from understanding how the research was done (e.g. sample size; level of replication; statistical analyses…); lack of novelty/only confirmatory of previously published work; interpretations that are not strongly supported by the data and/or that greatly overstep the constraints of the work.

In an era of ever-increasing numbers of scientific journals, particularly those publishing online, it is inevitable that the Journal receives manuscripts that may have been previously rejected by other journals. Although it is not a condition of submission, an open acknowledgement of prior rejection by another journal, together with the reasons, can assist the Journal’s review process. Manuscripts that are sent to a reviewer who has previously seen it for another journal - particularly if his/her advice has not been taken into account in preparing a revised version - very often attract negative review comments. Authors should consider this carefully when submitting a manuscript that has been rejected elsewhere.

# Instructions for online submission of manuscripts

Format-Free Submission

New submissions to ICES JMS do not have to be formatted in the Journal's style and format. Nonetheless, they should be organized in a manner that is consistent with a standard research article (including length) and be formatted so that they are easy to read and comment on (e.g. with line numbers, double line spacing…).

On revision, provisionally accepted manuscripts should be carefully formatted to the Journal's style.

Preparing Your Manuscript

Follow the instructions to authors regarding the format of your manuscript and references.
Prepare your manuscript, including tables, using a word processing program and save it as a .doc or .rtf file. All files in these formats will be converted to .pdf format upon submission.
Prepare your figures at publication quality resolution, using applications capable of generating high-resolution .tif files (1200 d.p.i. for line drawings and 300 d.p.i. for colour and half-tone artwork) or .jpg, .gif, .tif or .eps format. Please be sure to embed the fonts correctly in your eps and pdf files to ensure that they convert to pdf. The printing process requires your figures to be in this format if your paper is accepted and printed. For useful information on preparing your figures for publication, go to this page.
When preparing figures, please make sure that any characters or numbers included in the figures are large enough to read on-screen. Authors should size their figures to fit either into one column (85 mm wide) or double column/page width (170 mm wide), but preferably not in between.
Prepare any other files that are to be submitted for review, including any supplementary material. The permitted formats for these files are the same as for manuscripts and figures. Other file types, such as Microsoft Excel spreadsheets and Powerpoint presentations may be uploaded and will be converted to .pdf format.
When naming your files, please use simple filenames and avoid special characters and spaces. If you are a Macintosh user, you must also type the three-letter extension at the end of the file name you choose (e.g. .doc, .rtf, .jpg, .gif, .tif, .ppt, .xls, .pdf, .eps, .mov).
Submitting Your Manuscript

When your files are ready, visit the online submission web site.

Note: Before you begin, you should be sure you are using an up-to-date version of a common web browser. If you have an earlier version, you can download a free upgrade using the icons found at the bottom of the 'Instructions and Forms' section of the online submission web site.
First, you will need to log into the online submission site.
If you know your login details (i.e., you have submitted or reviewed a manuscript in this journal before), use your User ID and Password to log on. (Your user ID will usually be your email address.)
If you do not know your login details, check to see if you are already registered by clicking on the 'Forgot your password' button and following the on-screen instructions. If you are not already registered, you can register by clicking on the 'Create account' button on the login screen and following the on-screen instructions.
If you have trouble finding your manuscripts or have other problems with your account, do not create another account. Instead, please contact the journal's editorial office.
To submit a new manuscript, go to the 'Author Centre', and click on “Click here to submit a new manuscript', and then follow the on-screen instructions. There are up to 7 steps for you to follow to submit your manuscript. You move from one step to the next by clicking on the 'Next' button on each screen or back to the previous screen by clicking on the 'Previous' button. Please note that if you click on the 'Back' or 'Forward' buttons on your browser, the information you have entered will not be saved. At any stage you can stop the submission process by clicking on the 'Main Menu' button. Everything you have typed into the system will be saved, and the partially completed submission will appear under 'unsubmitted manuscripts' in your 'Author Centre'. To return to the submission process you will need to click on the button 'Continue Submission' against the relevant manuscript title.
When submitting your manuscript, please enter your manuscript data into the relevant fields, following the detailed instructions at the top of each page. You may like to have the original word-processing file available so you can copy and paste the title and abstract into the required fields. You will also be required to provide email addresses for your co-authors, so please have these to hand when you log onto the site.
When you come to upload your manuscript files via the 'File Upload' screen:
Enter individual files using the 'Browse' buttons and select the appropriate 'File type' from the pull-down menu. The choices may vary from journal to journal but will always include a 'Main Document' (your manuscript text).
Upload your files by clicking on the 'Upload files' button. This may take several minutes. Click on the SAVE button to confirm the upload. Repeat these steps until you have uploaded all your files.
If you have uploaded any figures or tables you will be prompted to provide figure/table captions and 'file tags' that will link figures to text in the HTML proof of your main document.
Once you have uploaded all your files, indicate the order in which they should appear in your paper. This will determine the order in which they appear in the consolidated PDF used for peer review.
After the successful upload of your text and images, you will need to view and proofread your manuscript. Please do this by clicking on the blue HTML button or a PDF button.
If the files have not been uploaded to your satisfaction, go back to the file upload screen where you can remove the files you do not want and repeat the process.
When you are satisfied with the uploaded manuscript proof click on 'Next' which will take you to the 'Review & Submit' screen. The system will check that you have completed all the mandatory fields and that you have viewed your manuscript proof. It will also present you with a summary of all the information you have provided and give you a final chance to edit it. If there is a red cross next to any section this will indicate that not all the fields have been filled in correctly. You may either go back to the relevant page or click the nearest 'edit' button.
When you have finished reviewing this information press 'Submit'.
After the manuscript has been submitted you will see a confirmation screen and receive an email confirmation stating that your manuscript has been successfully submitted. This will also give the assigned manuscript number, which is used in all correspondence during peer review. If you do not receive this, your manuscript will not have been successfully submitted to the journal and the paper cannot progress to peer review. If this is the case your manuscript will still be sitting in the 'Unsubmitted Manuscripts' section of your 'Author Centre' awaiting your attention.
If you return to your 'Author Centre' you will notice that your newly submitted manuscript can be found in the 'Submitted Manuscripts' area. The 'Status' section provides information on the status of your manuscript as it moves through the review process.
Submitting a Revised Manuscript

Logon to the online submission web site as before and, in the 'Author Centre', click on 'Manuscripts to be Revised'. You will then see the title of any manuscripts you submitted that are under revision.
If you click on 'View comments/respond' you will see the editor's letter to you together with the referees' comments. You may cut and paste your responses into the text areas at the bottom of the screen.
If you click on the manuscript title you will reach the 'File Manager' screen. Here you can upload the files that constitute your revised manuscript. To facilitate the production process, it is essential that you upload your revised manuscript as a .doc or .rtf file, and not in .pdf format.
IMPORTANT. Your images are required as high-resolution .tif files (1200 d.p.i. for line drawings and 300 d.p.i. for colour and half-tone artwork). For useful information on preparing your figures for publication, go to this page. Please note that publication of your manuscript will not proceed until figures suitable for reproduction are received.

Getting help
If you experience any problems during the online submission process please use the 'Author Help' function, which takes you to specific submission instructions, or 'Get Help Now', which takes you to the Frequently Asked Questions page. Alternatively, contact the Scholar One support line by email ( support@scholarone.com ) or telephone (+1 434 817 2040 x167).

You can also contact the ICES Journal of Marine Science editorial office (ices.editorialoffice@oup.com) by e-mail.
