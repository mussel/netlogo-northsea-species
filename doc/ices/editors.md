<!--
SPDX-FileCopyrightText: 2024 Oxford University Press
SPDX-FileContributor: Serra Örey
SPDX-FileContributor: Carsten Lemmen
SPDX-License-Identifier: CC0-1.0
-->

# Editor-in-Chief

Howard I. Browman
Institute of Marine Research,
Ecosystem Acoustics Group,
Austevoll Research Station,
Sauganeset 16, 5392 Storebø, Norway
Email: howard.browman@imr.no

# Editorial Office

Lynsey Rowland
ices.editorialoffice@oup.com

# Topical editors

Anne Beaudreau
University of Washington
School of Marine and Environmental Affairs
3707 Brooklyn Avenue NE
Seattle, WA
98105
USA
Email: annebeau@uw.edu
Areas of expertise: fisheries human dimensions; fishers’ knowledge; fishery management and policy; community-based participatory research; fisheries ecology; estuarine and marine fishes and food webs; mixed methods; transdisciplinary approaches

Jonathan H. Grabowski
Northeastern University,
Department of Marine and Environmental, Sciences Marine Science Center,
430 Nahant Road,
Nahant, MA 01908,
USA 92037-1508
Email: j.grabowski@neu.edu
Website
Areas of expertise: Social-ecological systems in fisheries and aquaculture, coastal habitat restoration and conservation, predator-prey ecology, fisheries ecology, ecosystem services, stakeholder engagement, perceptions, and attitudes, marine policy and management (coastal fisheries, habitat restoration, living shorelines), fish community ecology, migration and telemetry

Edward Hind-Ozan
Marine & Fisheries Directorate,
Department for Environment Food & Rural Affairs,
1-2 Peasholme Green, York, YO1 7PX,
United Kingdom
Email: edward.hind-ozan@defra.gov.uk
ORCID logohttps://orcid.org/0000-0002-7064-4931
Website
Areas of expertise: Socio-ecological systems, sociology, human geography, co-management, co-design, experiential knowledge, citizen science, participatory research, governance, coastal communities, science communication

Christos Maravelias
Associate Professor in Ichthyology
University of Thessaly (UTh), School of Agricultural Sciences (SAS)
Department of Ichthyology & Aquatic Environment (DIAE)
Fytokou str., 38446, Volos, Greece
Email: cmaravel@uth.gr
https://orcid.org/0000-0002-2440-3707
CV
Areas of expertise: Fisheries ecology and oceanography; fleet dynamics, climate change, ecosystem approach to fisheries, fish habitat, stock assessment, fisher behavior, traditional ecological knowledge, bioeconomic modeling, geostatistics, fisheries management.

Sean Pascoe
CSIRO Environment
Queensland Biosciences Precinct
306 Carmody Road
St Lucia 4067 Qld, Australia
Email: sean.pascoe@csiro.au
https://orcid.org/0000-0001-6581-2649
Website
​Areas of expertise: Fisheries economics; bioeconomic modelling; qualitative modelling; objective elicitation and weighting; management strategy evaluation; efficiency and productivity analysis; non-market valuation; demand modelling; incentives; fisher behaviour.

Raúl Prellezo
AZTI -Marine Research Division,
Txatxarramendi ugartea z/g
48395 Sukarrieta, Bizkaia,
Spain
Email: rprellezo@azti.es
ORCID logohttps://orcid.org/0000-0001-5998-8146
Website
Areas of expertise: Fisheries economics; economic modeling; fisheries sustainability; fisheries management; evaluation of management strategies; game theory.

Saša Raicevich
Department of Monitoring, Environmental Protection and Biodiversity Conservation
Istituto Superiore per la Protezione e la Ricerca Ambientale (ISPRA)
Loc. Brondolo, 30015 Chioggia
Italy
Email: sasa.raicevich@isprambiente
Areas of expertise: Marine historical ecology; fisheries ecology; fishing impact on benthic communities, discards, and ecosystems; small-scale fishing; participatory research; citizen science; fisheries management (ecosystem-based approach, co-management); policies for marine environmental protection and conservation.

Shareef Siddeek
Alaska Department of Fish and Game
Division of Commercial Fisheries
P. O. Box 115526
1255 W. 8th Street
Juneau, AK, USA, 99811-5526
Email: siddeekak7@gmail.com
CV
Areas of expertise: length-based stock assessment models; crab and shrimp fisheries biology and stock assessment; biological reference points; harvest control rules; tropical fish and shellfish stock assessment; utility of fisheries observer data in stock assessment​

Pamela J. Woods
Demersal Division
Marine and Freshwater Research Institute
Fornubúðir 5, 220 Hafnarfjörður, Iceland
Email: pamela.woods@hafogvatn.is
ORCID logohttps://orcid.org/0000-0001-6565-2841
Website
Areas of expertise: demersal fisheries; trophic interactions; intraspecific diversity; mixed fisheries; management strategy evaluation; stock assessment; fisheries management; fisheries policy; fleet dynamics; fisher behaviour; socioeconomics; climate change adaptation; resilience.
