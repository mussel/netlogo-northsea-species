---
title: "Lingua Franca Document for Viable North Sea (ViNoS): A NetLogo Agent-based Model of German Coastal Fisheries"
subtitle: "Open Modeling Foundation Standards Working Group"
keywords: "NetLogo; Agent-based Model; ABM; North Sea; Fisheries; MuSSeL project; ODD; VIABLE; ViNoS"
journal: omf
csl-refs: true
type: swg
classoption: moreauthors=false
status: accept
author:
  - name: Carsten Lemmen
    orcid: 0000-0003-3483-6036
correspondence: C. Lemmen <carsten.lemmen@hereon.de>
affiliation:
  - address: Helmholtz-Zentrum Hereon, Institute of Coastal Systems - Analysis and Modeling, Max-Planck-Str. 1, 21502 Geesthacht, Germany
    email: carsten.lemmen@hereon.de
citation_author: Lemmen
year: 2024
license: CC-BY-4.0
bibliography: Lemmen2024_linguafranca.bib
SPDX-FileCopyrightText: 2024 Helmholtz-Zentrum hereon GmbH
SPDX-License-Identifier: CC-BY-4.0
conflictsofinterests: "The authors declare that no conflict of interest has arisen from this work."
abbreviations:
  - short: ABM
    long: "Agent-based Model"
  - short: ODD
    long: "Overview, Design concepts, Details"
  - short: DES
    long: "Discrete Event Simulation"
  - short: EEZ
    long: "Exclusive Economic Zone"
  - short: FAO
    long: "United Nations Food and Agricultural Organization"
  - short: GEBCO
    long: "General Bathymetry Chart of the Oceans"
  - short: MuSSeL
    long: "Multiple Stressors on North Sea Life"
  - short: ViNoS
    long: "Viable North Sea"
  - short: VIABLE
    long: "Values and Investments from Agent-Based interaction and Learning in Environmental systems"
abstract: "Viable North Sea is an Agent-based Model of the German North Sea Coastal Fisheries.  I here document the model in a condensed version of the  Overview, Design concepts, Details protocol as a contribution to the OMF Standards Working Group effort on identifying a Lingua Franca for modeling."
acknowledgement: "The author acknowledges the Open Modeling Foundation's Standards Working Group Members for stimulating discussion leading to this manuscript.  The development of ViNoS has been funded by the German Ministry of Education and Research through the KüNO project 'Multiple Stressors on North Sea Life' (MuSSeL) with grant number 03F0862A.  I am grateful for the open source communities that facilitate this research, amongst them the developers of and contributors to NetLogo, Python, R, pandoc, LaTeX, and many others."
---

# German North Sea Coastal Fisheries

Viable North Sea (ViNoS) is an Agent-based Model (ABM) of the German North Sea Coastal Fisheries in a Social-Ecological Systems (SES) framework focussing on the adaptive behaviour of fishers facing regulatory, economic, and resource changes [@Lemmen2024]. This simplified model description follows the Overview, Design concepts, Details (ODD) protocol for describing individual- and agent-based models (@Grimm2006;@Grimm2010), in a condensed form consistent with Supplement S2 of @Grimm2020.

# Condensed ODD

**A complete, detailed model description, is provided** as part of the Comses.net model deposition [@Lemmen2023], and as a living document in the model's openly accessible source tree at https://codebase.helmholtz.cloud/mussel/netlogo-northsea-species/-/blob/main/doc/odd/odd.md. The **overall purpose of the model** is to provide an interactive simulation environment that describes spatial, temporal and structural adaptations of the German coastal fishery fleet. **Specifically, the following questions are addressed:** What happens to the fleet's economy, structure, and fishing pattern when traditional fishing grounds are closed for increasing offshore energy provisioning or nature protection? What happens when oil prices and wages increase, or when the resources shift more offshore with climate heating?
**To consider our model realistic enough for its purpose, we use patterns** in surface area ratio (SAR) and fishing effort, monthly fleet landings and income.

**The model includes the following entities**: boats, gears, preys, ports, and actions and patches: The primary entities in the ABM are some 300 fishing vessels, denoted as _boats_. They are linked to supplementary entities that describe the 7 different _gears_ used for fishing, the 3 target species denoted as _preys_, and the 50 _ports_ where boats land their catch. The agents interact with the _patches_ environment through resource and regulatory changes. **The state variables characterizing these entities are listed in Table 1**.

**The spatial resolution and extent** is the German Bight, part of the North Sea, between (2° E; 53° N) and (10° E; 56° N); the resolution is 0.025 x 0.025 degree (1.5 arc minutes, or approximately 1.7 x 2.9 km). The resulting grid has a size of 320 columns x 120 rows. **The temporal resolution and extent** is are annual to multi-annual simulations between 1950 and 2050 at daily model time step. A finer temporal resolution is achieved by Discrete Event Simulation at up to hourly resolution.

| Entity            | Variable                             | Description                                                      | Possible Values    | Units    |
| ----------------- | ------------------------------------ | ---------------------------------------------------------------- | ------------------ | -------- |
| Boat              | boat-capacity                        | Storage size                                                     | 0 - 20000          | kg       |
| Boat              | boat-steaming-speed                  | Speed when steaming                                              | 0 - 30             | km h^-1^ |
| Boat              | boat-landing-port                    | Favorite landing port                                            | port (1)           | --       |
| Boat              | boat-home-port                       | Home port                                                        | port (1)           | --       |
| Boat              | boat-gears                           | Available boat gears                                             | gears (1..7)       | --       |
| Boat              | boat-type                            | Activity cluster the boat belongs to                             | 1 - 4              | --       |
| Boat              | boat-engine                          | power of engine                                                  | 0 - 1000           | kW       |
| Boat              | boat-length                          | Length of boat                                                   | 0-30               | m        |
| Boat              | boat-name                            | Name                                                             | any text           | --       |
| Boat              | boat-captain                         | Captain's name                                                   | any text           | --       |
| Boat              | boat-is-learning?                    | In learning phase                                                | true/false         | --       |
| Boat              | boat-captial                         | Accumulated capital                                              | any number         | €        |
| Boat              | boat-max-distance                    | Maximum preferred distance from port                             | 0 - 1000           | km       |
| Boat              | boat-trip-gear-catches               | List of fish catches of trip                                     | 0 - 20000          | kg       |
| Boat              | catch-efficiency-boat                | Fraction of efficiency                                           | 0 - 1              | --       |
| Boat              | revenue-boat                         | Fishing trip revenue                                             | 0 - 10000          | €        |
| Boat              | costs-boat                           | Fishing trip costs                                               | 0 - 10000          | €        |
| Boat              | boat-gains                           | Fishing trip gains                                               | 0 - 10000          | €        |
| Boat              | revenue-boat                         | Fishing trip revenue                                             | any number         | €        |
| Boat              | boat-delta-gains                     | Change in gain                                                   | any number         | €        |
| Boat              | boat-gear-priorities                 | List of relative priorities                                      | 0 - 1              | --       |
| Boat              | boat-delta-priorities                | List of relative priority changes                                | 0 - 1              | --       |
| Boat              | priority-weighted-average            | priority weighted average of gains                               | any number         | €        |
| Boat              | <prey>-catch-kg                      | Fishing trip catch of <prey>                                     | 0 -- 20000         | kg       |
| Boat              | <prey>-catch-euro                    | Fishing trip catch of <prey>                                     | _float_ >= 0       | €        |
| Boat              | boat-time-at-sea                     | Fishing trip expired time                                        | _float_            | h        |
| Boat              | boat-time-at-sea-left                | Fishing trip time left                                           | _float_            | h        |
| Boat              | boat-distance-at-sea                 | Fishing trip expired distance                                    | \_float\_\_        | km       |
| Boat              | boat-trip-phase                      | Index of phase                                                   | 0 - 6              | --       |
| Boat              | boat-trip-best-patch                 | Trip's highest catch patch                                       | _patch_            | --       |
| Boat              | boat-trip-best-catch                 | Trip's highest catch                                             | _float_            | kg       |
| Boat              | boat-needs-return?                   | Indicates necessity to return                                    | true/false         | --       |
| Boat              | boat-hour                            | Hour of day for DES                                              | 0 - 24             | h        |
| Boat              | boat-fishable-patches                | Fishable patches                                                 | _patches_          | --       |
| Boat              | boat-actions                         | Set of actions                                                   | _actions_          | --       |
| Boat              | boat-preys                           | Set of preys                                                     | { _preys_ }        | --       |
| Boat              | boat-current-pear-index              | Index of current gear                                            | _int_              | --       |
| Boat              | boat-current-prey-index              | Index of current prey                                            | _int_              | --       |
| Boat              | boat-total-time-at-sea               | Accumulated time at sea                                          | _float_            | h        |
| Boat              | boat-total-fuel                      | Accumulated fuel consumption                                     | _float_            | l        |
| Boat              | boat-total-landings                  | Accumulated landings                                             | _float_            | kg       |
| Boat              | boat-logbook                         | Logbook kept by a boat                                           | _logbook_          | --       |
| Port              | port-kind                            | Type of port                                                     | "home", "favorite" |          |
| Port              | port-name                            | Full name according to UN_LOCODE                                 | _string_           |          |
| Port              | port-country                         | ISO3 Country Code                                                | _string_           |          |
| Port              | port-latitude                        | Geographic latitude                                              | 0..360             | °N       |
| Port              | port-longitude                       | Geographic latitude                                              | -180..180          | °E       |
| Port              | port-start-patch                     | Neareast navigable patch                                         | _patch_            |          |
| Port              | port-landing-patch                   | Patch for landing                                                | _patch_            |          |
| Port              | port-fish-catch-kg                   | List of catches                                                  | [ _float_ ]        | kg       |
| Port              | port-prey-landed                     | List of prey names                                               | [ _text_ ]         |          |
| Port              | port-transportation-costs            | Average transportation costs as percentage of the total landings | _float_            | €        |
| Port              | port-operation-costs                 | Average operating costs as percentage of the total landings      | _float_            | €        |
| Port              | port-average-trip-length             | Average trip length                                              | _float_            | h        |
| Port              | port-average-fishing-effort-in-days  | Average fishing effort                                           | _float_            | d        |
| Port              | port-average-fishing-effort-in-hours | Average fishing effort                                           | _float_            | h        |
| Port              | port-weather                         | Status of the weather                                            | "bad"/"good"       |          |
| Port              | port-prob-bad-weather                | Probability that weather is too bad                              | 0..1.0             |          |
| Port              | port-prey-names                      | List prey names                                                  | [ _text_ ]         |          |
| Port              | port-clusters                        | List of cluster indices in port                                  | [ _int_ ]          |          |
| Prey              | prey-name                            | Uppercase English official name                                  | _text_             |          |
| Prey              | prey-scientific                      | Scientific name                                                  | _text_             |          |
| Prey prey-english | English common name                  | _text_                                                           |                    |
| Prey              | prey-german                          | German common name                                               |                    |          |
| Prey              | prey-three-code                      | FAO 3 code                                                       | _text_             |          |
| Prey              | prey-picture                         | URL pointing to resource file                                    | _text_             |          |
| Gear              | gear-name                            | Name                                                             | _text_             |          |
| Gear              | gear-prey-name                       | Name of prey                                                     | _text_             |          |
| Gear              | gear-prey                            | Prey                                                             | _prey_             |
| Gear              | gear-width                           | Width                                                            | _float_            | m        |
| Gear              | gear-drag                            | Drag (unused)                                                    |                    |          |
| Gear              | gear-installation-cost               | Cost of changing                                                 | _float_            | €        |
| Gear              | gear-purchase-cost                   | Investement cost                                                 | \_float            | €        |
| Gear              | gear-speed                           | Speed when deployed                                              | _float_            | km h^-1^ |
| Gear              | gear-efficancy                       | Efficacy of catch                                                | 0..1.0             |          |
| Action            | action-catch                         | Catch of haul                                                    | _float_            | kg       |
| Action            | action-gear                          | Gear used                                                        | _gear_             |          |
| Action            | action-gain                          | Temporary expected gain                                          | _float_            | €        |
| Action            | action-age                           | Time since last used                                             | _float_            | h        |

**The most important processes of the model, which are repeated every time step**, are
environmental updates and activities of the boats. In the environment the prey resources relax towards their seasonal climatology, tides change the water depth and accessibility of ports, and closure areas may be introduced. Boats cycle event-based through 6 phases (Table 2).

| **Phase**     | **Location** | **Description**                            |
| ------------- | ------------ | ------------------------------------------ |
| Phase 0 rest  | port         | Boats are resting, not ready to go out     |
| Phase 1 ready | port-sea     | Boats are ready to go out and are deployed |
| Phase 2 steam | sea          | Boats steam in open water                  |
| Phase 3 fish  | sea          | Boats fish open water                      |
| Phase 4 steam | sea          | Boats need to return                       |
| Phase 5 land  | port         | Boats are offloading                       |

**The most important design concepts** of the model are (1) the constraint-based decisions taken during a fishing trip, leading to an emergent pattern of often fished patches; this learned pattern of viable actions can be well compared to existing data on fishing effort; and (2) the shift of priorities for gears based on the experiences of the agents, which virtually catch (and record the success of) all prey in a hauled area and thereby learn to optimize the gear used together with the fishing grounds; this optimization follows the Values and Investments from Agent-Based interaction and Learning in Environmental systems approch (VIABLE, @BenDor2019; @Scheffran2000).

**Model dynamics are driven by input data** representing a multitude of environmental and fleet statistics data (Table 3).

| **Description**                                        | **Source**                                                                            |
| ------------------------------------------------------ | ------------------------------------------------------------------------------------- |
| Clustered vessel data                                  | Hochschule Bremerhaven                                                                |
| Species distribution of plaice, sole, and brown shrimp | Thünen Institute                                                                      |
| Species information                                    | Food and Agriculture Organization (FAO)                                               |
| Bathymetry                                             | General Bathymetry Chart of the Oceans (GEBCO)                                        |
| Offshore Wind Farms (OWF)                              | European Commission, European Marine Observation and Data Network (EMODnet)           |
| Exclusive economic zone (EEZ)                          | United Nations Convention on the Law of the Sea                                       |
| Subregional divisions                                  | International Council for the Exploration of the Seas (ICES) Spatial Facility         |
| Plaice box                                             | European Commission                                                                   |
| Geodetic information                                   | International Earth Rotation Service (IERS)                                           |
| National Park boundaries                               | Niedersächsischer Landesbetrieb für Wasserwirtschaft, Küsten- und Naturschutz (NLWKN) |
| National Park boundaries                               | LLUR Schleswig Holstein                                                               |
| Cargo shipping activity                                | Bundesamt für Seeschiffahrt                                                           |
| Historical fleet landings and revenues                 | Bundesministerium für Landwirtschaft und Ernährung                                    |

# CRediT authorship contribution statement

C. Lemmen: Conceptualization, Methodology, Resources, Software, Formal analysis, Data curation, Project administration, Writing – original draft, Writing – review & editing.

# References
