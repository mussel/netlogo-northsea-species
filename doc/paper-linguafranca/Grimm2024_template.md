<!--
SPDX-FileContributor: Volker Grimm
SPDX-LicenseIdentifier: CC0-1.0
SPDX-FileCopyrightText: 2024 Helmholtz-Zentrum für Umweltforschung
-->

# Lingua Franca

Here a template is provided for describing the overall purpose, structure, scales, and processes of a model, without going into any detail regarding the processe. Also, the empirical patterns or observations used to claim that the model is realistic enough for its purpose should be mentioned.

This template is taken from:

> Supplement S2 to: Grimm V, Railsback SF, Vincenot CE, Berger U, Gallagher C, DeAngelis DL, Edmonds B, Ge J, Giske J, Groeneveld J, Johnston ASA, Milles A, Nabe-Nielsen J, Polhill JG, Radchuk V, Rohwäder MS, Stillman R, Thiele JC, Ayllón D. 2019. The ODD protocol for describing agent-based and other simulation models: a second update to improve clarity, replication, and structural realism.

The full ODD is described in Grimm et al. (2020), and guidance for using this format is the Supplement S1.

If this condensed ODD still is considered too formal to present the model’s narrative, the summary ODD can also start with a summary which is independent of ODD, but then be followed by the elements which are condensed from the full ODD. Please try and use the phrases and words in bold font in the template, which will help giving summary ODDs a similar structure. Please keep formatting of ODD’s key words in italics in the final summary ODD. Example summary ODDs from ecology are provided below.

This is the template - please copy and paste to a Word file or so, and try to fill in your own text and send that file to volker.grimm@ufz.de.

## TEMPLATE

A complete, detailed model description, is provided at <Insert to paper or web repository where the full model description can be found. Please send this paper or so together with the summary ODD>.

<Optional: start with a free-form summary: > The basic idea underlying the model is ... < …>

The overall purpose of our model is … <summarize the corresponding part of your full model description>. Specifically, we are addressing the following questions: … <summarize the corresponding part of your full model description>. To consider our model realistic enough for its purpose, we use the following patterns … >.
As for purpose, you might distinguish 1 demonstration (of ideas or concepts), 2 understanding, and 3 – prediction. Or, add the more refined distinction of model purposes listed by Edmonds et al. (2021)

The model includes the following entities … <list entities>. They are characterized by the following state variables …/The state variables characterizing these entities are listed in Table X: ... <Ideally, present a Table with model entities and their state variables (see Supplement S1 for further guidance). For experienced modellers, such a table provides a good overview of the model’s structure and scope>. The spatial and temporal resolution and extent are …

The most important processes of the model, which are repeated every time step, are ... <Use the sequence of the model’s schedule. If possible, use the same self-explaining names, or similar, of your full model description. Use one or two senentences summarizing what that process is, i.e. what actually is happening – without going into detail. For key submodels, you might present them in some more detail at the end of the summary ODD. Try to say, who is executing the process, just the model (you, or the observer), or whether it is run by a certain entity, e.g. an agent or a grid cell>.
If your model is formulated as a set of coupled differential equations, there is of course no scheduling, but still, even such models are often using if-then conditions for activating or deactivating certain processes.

<Optional: list aspects of Initialization and Input data, if they are essential: > The model is initialized with ... <Specific initial conditions>. Model dynamics are driven by input data representing ... <What kind of data, and what processes are they driving? What is the source of the data?>.

<Optional: explain here in more detail submodels that are too complex to be described with one or two sentences in the process overview above: > Key processes in the model are ... <Refer to each of the processes you want to describe here by the name you used in the overview above, set in italics. Give a verbal summary description, but also include equations if they are essential for understanding.>

## Examples

### Tiger model by Carter al. 2015.

The full ODD is in the main text of Carter, N., Levin, S., Barlow, A., & Grimm, V. (2015). Modeling tiger population and territory dynamics using an agent-based approach. Ecological Modelling, 312, 347-362. This summary ODD was written for this template. Text suggested by the template is underlined to demonstrate the use of the template, but would not be hightlighted in real summary ODDs.

A complete, detailed model description, following the ODD (Overview, Design concepts, Details) protocol (Grimm et al. 2006, 2010, 2019) is provided in Carter et al. (2015). The overall purpose of our model is to predict tiger population dynamics to support tiger conservation. Specifically, we are addressing the following question: How does the number, location, and size of tiger territories change in response to habitat quality and tiger density? To consider our model realistic enough for its purpose, we use patterns in mortality, age structure, and female territory tenure and overlap of male territories with female territories.
The model includes the following entities: tigers, square grid cells of 250x250 m², and territories. The state variables characterizing these entities are listed in Table 1. Female territories consist of a set of habitat cells, which the females add to their territory based on prey availability and presence and rank of other females. Male territories consist of a set of up to six female territories, which the males add to their territory based on their spatial proximity and absence or rank of other males. As for the spatial and temporal resolution and extent: A time step in the model represents one month and simulations are run for 1-20 years. Test simulations were run on landscape of 40x40 and 128x128 cells, while the Chitwan national park required 157x345 cells. Landscape structure in terms of prey availability is assumed to be constant in this model version, but will change in response to human land use in future model applications.
The most important processes of the model, which are repeated every time step, are the acquisition of territories by dispersing young tigers, and the maintenance of territories by established tigers. Female tigers add grid cells to their territory until they can satisfy their monthly prey requirement. Grid cells can be won from or lost to other females. Male tigers do not strive to add space based on prey availability, but on the availability and location of female territories. They try to overlap up to six female territories, but compete for these with each other, depending on their relative age and rank; the latter depends on the outcome of previous contests. If a female territory is won from another male, infanticide can occur, which can add substantially to juvenile mortality.
The most important design concept of the model is the way in which we represented territorial dynamics, in particular the response of females to prey availability, and of males to the distribution of both female and male residents. Thereby we let population structure and dynamics emerge from the prey distribution in the landscape and from overall tiger density.

Table 1. Entities and state variables of the tiger model by Carter et al. (2015).
Entity
Variable
Description
Possible Values
Units
Female
Age
Age in months
1 - 180
Months

fertile?
Indicates whether female is fertile
True/False
--

gestating?
Indicates whether female is gestating
True/False
--

males-in-my-territory
Identities of males overlapping female territory
Set of male identities
--

my-mom
Identity of mom
Identity of female tiger
--

my-offspring
Number of offspring in current litter
1 - 5
Individual cubs

natal-origin
Patch where female was initialized at or the centroid patch of mother's territory
0 - max X, 0 - max Y
Patch units

num-litters
Total number of litters the female has had up until current time
0 - max number of litters over lifetime
--

age-class
Indicates development stage of female
Cub, Juvenile, Transient, or Breeder
--

territory
Set of patches belonging to territory
Set of patch coordinates
--

terr-orig
Patch that female was initialized at or first patch of territory
0 - max X, 0 - max Y
Patch units

t-gestation
Indicates how long female has gestated
0 - 3 or 4
Months

t-parenting
Indicates how long female has been a parent of current litter
0 - 24
Months
Male
Age
Age in months
1 - 180
Months

dominant-males
Identities of males that have beaten male in challenges
Set of male identities
--

females-in-my-territory
Identities of females overlapping female territory
Set of female identities
--

initial-male?
Indicates whether male was created at beginning of simulation
True/False
--

lost-territory?
Indicates if male lost a territory to a challenger
True/False
--

male-land-tenure
Total time that male held onto territory
0 - entire breeding phase until death
Months

my-mom
Identity of mom
Identity of female tiger
--

natal-origin
Patch where male was initialized at or the centroid patch of mother's territory
0 - max X, 0 - max Y
Patch units

age-class
Indicates development stage of male
Cub, Juvenile, Transient, or Breeder
--

territory
Set of patches belonging to territory
Set of patch coordinates
--
Cell
owner-fem
Identity of female with patch in her territory
Identity of female tiger
--

owner-male
Identity of male with patch in his territory
Identity of male tiger
--

Prey
Prey produced at patch
0 - max prey production
kg/month

is-churia?
Indicates whether patch falls within churia hill boundary (Chitwan landscape only)
True/false
--

is-park?
Indicates whether patch falls within national park boundary (Chitwan landscape only)
True/false
--

Trout model by Ayllón al. 2016.

The full ODD is in the supplementary material of Ayllón, D., Railsback, S.F., Vincenzi, S., Groeneveld, J., Almodóvar, A., & Grimm, V. (2016). InSTREAM-Gen: Modelling eco-evolutionary dynamics of trout populations under anthropogenic environmental change. Ecological Modelling, 326: 36-53. This summary ODD was written for this template.

A complete, detailed model description, following the ODD (Overview, Design concepts, Details) protocol (Grimm et al. 2006, 2010, 2019) is provided in the supplementary material of Ayllón et al. (2016). The overall purpose of our model is to understand how environmental conditions and anthropogenic disturbances drive the evolution of demographics and life-history strategies of stream-dwelling trout populations. Specifically, we are addressing the following question: How do trout population demographic and evolutionary dynamics change in response to changes in environmental drivers caused by climate and land use change? To consider our model realistic enough for its purpose, we use patterns of habitat selection and population dynamics, including quantitative patterns of age-specific population changes over time and qualitative population-level phenomena (e.g., self-thinning, density-dependent growth). The model’s ability to reproduce a variety of patterns in adaptive habitat selection behavior was demonstrated by Railsback & Harvey (2002) and its ability to reproduce observed patterns in population dynamics was demonstrated by Railsback et al. (2002).
The model includes the following entities: cells, trout, and redds. The state variables and attributes characterizing these entities are listed in Table 2. The global environment (a stream reach) is characterized by water temperature and flow. Cells represent patches of relatively uniform habitat within the reach, and are characterized by both their physical habitat (mean depth and water velocity that are flow-dependent, area of velocity shelter for drift feeding, spawning gravel area, and mean distance to hiding cover), and their production rate of drift and benthic food. Cells are rectangles in the horizontal plane (habitat variability in the vertical dimension is ignored) of nonuniform size and shape. Trout have unique values of body length, weight and condition, and both phenotypic and genotypic values for the heritable traits. Redds are spawning nests made by trout, which have variables for the number and development status of the eggs they contain. Redds also carry the genetic information of the female spawner who created the redd and of the male spawners who fertilized the eggs. As for the spatial and temporal resolution and extent: A time step in the model represents one day and simulations are run for a length set by the input environmental time series. The model is spatially explicit and describes one reach of a stream, typically a few hundred meters in length. Both spatial extent and resolution are set by the user. The length of the reach simulated in this study is 305 meters, having 220 cells at an average flow, with a total area of 2390 m2.
The most important processes of the model, which are repeated every time step, are the update of environmental conditions in the reach and cells’ flow-dependent variables, and the following trout actions: (1) All trout select habitat following a size-based dominance hierarchy that gives larger trout first access to food and preferred habitat. In order from largest to smallest, each trout moves to the available cell–within a radius that increases with trout lengththat–maximizes short-term fitness, which is a function of the cell’s mortality risk and growth potential; (2) Trout feed and grow according to their food intake and energy costs experienced in their cell, which are calculated through a bioenergetics model; (3) All trout are subject to six natural sources of mortality (high temperatures, high flow velocity, stranding, starvation, predation by terrestrial animals, and predation by piscivorous trout). Only during the spawning (i.e., reproductive) season, mature females spawn if both environmental and internal conditions are met. If so, female trout create a redd and its eggs are fertilized by the largest available male spawner plus a random number of smaller mature males. The number of eggs increases exponentially with female length and also varies inversely with egg size. Egg size increases with the genotypic value of the female’s trait for size at emergence. Each redd stores the genetic information of the mother and all contributing males. Redds are subject to egg mortality and surviving eggs develop at a rate that increases with temperature. When eggs are fully developed, they hatch into new trout (emerge) that inherit the redd’s traits.
The most important design concepts of the model are adaptation, objectives and prediction. Habitat selection is the key adaptive behavior, which is modeled according to the “state- and prediction-based” foraging theory (Railsback and Harvey 2013), an approach that combines existing tradeoff methods with routine updating: when choosing their habitat, trout make a prediction of the future growth and risk conditions offered by each cell over an entire time horizon under different alternative behaviors (e.g., feeding strategy), but each time they update their decision by considering how their internal state and external conditions have changed, so that they can select the alternative optimizing an objective measure called "Expected Maturity" (EM; Railsback et al. 1999). EM represents the expected probability of surviving both predation and starvation over the time horizon, multiplied by a term representing how growth affects fitness. Thus, habitat and feeding-strategy selection strongly drive growth and survival of individuals. Trout are also able to adapt some of their reproductive behaviors to environmental conditions and their own state. Dynamics of population demography and trajectory of life-history and genetic traits thus emerge from the growth, survival, and reproduction of individuals, individual-level processes that are driven by environmental conditions and adaptive decisions.
Model dynamics are driven by input data representing daily time series of water temperature and flow.
A key process in the model is also the transmission of heritable traits. Each new trout inherits its genetic traits from the mother and one father randomly selected from the males that fertilized the redd, with equal probability of fertilization across males. We model the phenotype of an individual as the sum of an inherited additive genetic effect (genotypic value) and a non-heritable environmental effect, being the inheritance rules based on the infinitesimal model of quantitative genetics (Lynch and Walsh 1998).

Table 1. Entities and state variables of the trout model by Ayllón et al. (2016).
Agent
Variable
Description
Unit
Cells
cellArea\*
Area of the cell
cm2

CellAreaCover\*
Area of the cell with cover
cm2

cellAreaShelter\*
Area of the cell with velocity shelters
cm2

cellDepth
Value of depth at specific time
cm

cellDistanceToHide
Average distance from hiding cover from the cell’s center
cm

cellFracCover\*
Fraction of the cell with cover
Unitless (0-1)

cellFracGravel\*
Fraction of the cell with spawning gravel
Unitless (0-1)

cellFracShelter\*
Fraction of the cell with velocity shelters
Unitless (0-1)

cellFracSpawn\*
Fraction of the cell with spawning gravel
Unitless (0-1)

cellNumber\*
Number of the cell
number

cellVelocity
Value of velocity at specific time
cm s-1

driftHourlyCellTotal
Production rate of drift food items
g h-1

my-adjacentCells\*
Adjacent cells
agents

my-patches\*
Patches composing the cell
agents

searchHourlyCellTotal
Production rate of search food items
g h-1

Transect*
Transect where the cell is located
number
Redds
creationDate*
Date when the redd is created
date

days-after-hatch
Number of days since emergence starts in the redd
days

eggsLostToDewateringTot
Total number of eggs lost due to scouring
eggs

eggsLostToHighTempTot
Total number of eggs lost due to high water temperatures
eggs

eggsLostToLowTempTot
Total number of eggs lost due to low water temperatures
eggs

eggsLostToScourTot
Total number of eggs lost due to dewatering
eggs

eggsLostToSuperimpTot
Total number of eggs lost due to superimposition of redds
eggs

fracDeveloped
Developmental status of a redd’s eggs
Unitless (0-1)

my-cell\*
Cell where the redd is located
cell-id

numberOfEggs
Number of eggs in the redd
eggs

numberOfHatchedEggs
Number of eggs hatched (creating a new trout)
eggs

reddFathersgenNeutralTrait\*
Genotypic values of neutral trait of fathers
User-specific

reddFathersgenNewlength\*
Genotypic length at emergence of fathers
cm

reddFathersgenSpawnMinLength\*
Genotypic minimum length to spawn of fathers
cm

ReddID \*
Identity number of the redd
id

reddMothergenNeutralTrait\*
Genotypic value of neutral trait of the mother
User-specific

reddMothergenNewlength\*
Genotypic length at emergence of the mother
cm

reddMothergenSpawnMinLength\*
Genotypic minimum length to spawn of the mother
cm
Trout
age
Number of days since the fish was born
days

age-class
Age class
Age0-Age5

CauseOfDeath\*
Mortality source by which the fish is dead
8 sources

cMax
Physiological maximum daily intake
g d-1

energyAvailableforGrowth
Net energy gain in my-cell during the time step
J d-1

fishCondition
Condition factor
Unitless (0-1)

fishLength
Body length
cm

fishMaxSwimSpeed
Maximum sustainable swimming speed
cm s-1

fishNeutralTrait\*
Phenotypic value of the neutral trait
User-specific

fishNewLength\*
Phenotypic length at emergence
cm

fishSpawnMinLength\*
Minimum length to spawn
cm

fishWeight
Body weight
g

genNeutralTrait\*
Genotypic value of the neutral trait
User-specific

genNewLength\*
Genotypic length at emergence
cm

genSpawnMinLength\*
Genotypic minimum length to spawn
cm

is-sheltered?
Access to a velocity shelter
True/False

maturityStatus
Maturity status
mature/non-mature

my-cell
Cell where the fish is located
cell-id

sex\*
Sex
M/F

spawnedThisSeason?
Spawned this spawning season
true/false

status
Status
alive/dead
