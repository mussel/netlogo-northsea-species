<!--
SPDX-FileCopyrightText: 2022-2024 Helmholtz-Zentrum hereon GmbH
SPDX-FileContributor: Carsten Lemmen <carsten.lemmen@hereon.de>
SPDX-License-Identifier: CC0-1.0
-->

This directory contains documentation information, organized in folders, some of them organized in external Git submodules.

If you cannot see them, then try `git submodule update --recursive`

1. `./odd/` The Overview, Design, Details protocol for the NetLogo model
2. `./paper-joss/` The Journal of Open Source Software publication for describing the NetLogo model
3. `.comses/` The COMSeS model submission and zip creation
4. `./sesmo/` The Socio-Environmental Systems Modeling manuscript for fishery distance optimization
5. `./talk-sylt-2023/` A talk given on Sylt Sep 2023
6. `./paper-modelingpractise/` A paper on Good Modeling Software Practise by Lemmen and Sommer
7. `./paper-assumptions/` The assumptions document for ViNoS

Additional documentation can be found externally:

- Storymap "A shrimper's virtual logbook", [https://mussel-project.de/portal/apps/sites/#/mussel/apps/0ba2ad702307419cb7ba16a9e7a3c798/explore](https://mussel-project.de/portal/apps/sites/#/mussel/apps/0ba2ad702307419cb7ba16a9e7a3c798/explore)
