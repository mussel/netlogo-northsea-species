# SPDX-FileCopyrightText: 2024 Helmholtz-Zentrum hereon GmbH
# SPDX-License-Identifier: CC0-1.0
# SPDX-FileContributor: Carsten Lemmen <carsten.lemmen@hereon.de

import sys
import geopandas as gpd
import pathlib
import shapely
import pandas as pd


def points_to_polygons(gdf):
    areas = gdf.Area.unique()
    gdfs = []
    polygons = []

    for i, area in enumerate(areas):
        sdf = gdf[gdf["Area"] == area]
        points = sdf.geometry.to_list()
        polygons.append(points_to_polygon(points))
        gdfs.append(gpd.GeoDataFrame(geometry=[polygons[i]]))

        for c in sdf.drop(columns=["geometry", "Point"]).columns:
            gdfs[i][c] = sdf[c].unique()[0]
            gdfs[i]["Points"] = str(sdf.Point.to_list())

    return gdfs


def points_to_polygon(points):
    # Ensure the collection of points is closed
    if points[0] != points[-1]:
        points.append(points[0])

    # In the specification for shapefiles, ESRII states that closed rings
    # are those where points are ordered clockwise, and holes are ordered
    # anticlockwise.

    # Create a polygon from the points
    polygon = shapely.Polygon(points)
    return polygon


def csv_to_points(gdf):
    # Set the geometry using the 'Lat' and 'Lon' columns
    # Set the coordinate reference system if known (e.g., WGS84)
    gdf["geometry"] = gpd.points_from_xy(gdf["Lon"], gdf["Lat"])
    gdf.crs = "+proj=longlat +ellps=WGS84 +datum=WGS84 +no_defs"
    gdf = gdf.drop(columns=["Latitude N", "Longitude E", "Lat", "Lon"])

    return gdf


def main(input_file):
    points_file = pathlib.Path(str(input_file.parent / input_file.stem) + ".points.shp")
    gdf = gpd.read_file(input_file)
    gdf = csv_to_points(gdf)
    gdf.to_file(points_file, driver="ESRI Shapefile")

    polygon_file = pathlib.Path(
        str(input_file.parent / input_file.stem) + ".polygons.shp"
    )
    gdfs = points_to_polygons(gdf)
    gdf_polygon = pd.concat(gdfs, ignore_index=True)

    gdf_polygon.to_file(polygon_file, driver="ESRI Shapefile")
    print(f"POlygon shapefile {polygon_file} saved.")

    return gdf_polygon


if __name__ == "__main__":
    if len(sys.argv) != 2:
        input_file = pathlib.Path("../data/eu/management_zones_sylt.csv")
    else:
        input_file = pathlib.Path(sys.argv[1])

    gdf = main(input_file)
