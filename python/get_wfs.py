# -*- coding: utf-8 -*-
"""
This script creates from the BFN WFS a shapefile with fishery restrictions

SPDX-FileCopyrightText: 2024 Helmholtz-Zentrum hereon GmbH
SPDX-License-Identifier: CC0-1.0
SPDX-FileContributor: Carsten Lemmen <carsten.lemmen@hereon.de>
"""

import geopandas as gpd
from owslib.wfs import WebFeatureService

# Define the WFS URL and layer name
wfs_url = "https://geodienste.bfn.de/ogc/wfs/awz_nsg_fischereimassnahmen?SERVICE=WFS"


# Initialize the WFS service
wfs = WebFeatureService(url=wfs_url, version="2.0.0")

# for layer in list(wfs.contents):
#    print(layer)

layer_name = "bfn_mns_AWZ_NSG_Fischereimassnahmen:Delegierte_Verordnung_EU_2023_340_der_Kommission_vom_8._Dezember_2022"  # Replace 'layer' with the actual layer name


response = wfs.getfeature(typename=layer_name, outputFormat="SHAPE+ZIP")

# Load the response into a GeoDataFrame
# and convert German comma to point in float numbers
gdf = gpd.read_file(response)
for col in gdf.columns:
    if gdf[col].dtype == "object":
        try:
            gdf[col] = gdf[col].str.replace(",", ".").astype(float)
        except ValueError:
            pass  # If conversion fails, keep the original values


output_shapefile = "awz_nsg_fischereimassnahmen.shp"
gdf.to_file(output_shapefile)

print(f"Shapefile saved as {output_shapefile}")
