#! /usr/bin/env bash
# SPDX-FileCopyrightText: 2024 Helmholtz-Zentrum hereon GmbH
# SPDX-License-Identifier: CC0-1.0
# SPDX-FileContributor: Carsten Lemmen <carsten.lemmen@hereon.de

variables=('shrimp' 'sole'
'traffic' 'accessibility' 'area' 'bathymetry' 'emodnet_effort'
'plaice' 'plaicebox' 'shore_proximity')

for V in "${variables[@]}"; do
    P=../netlogo/results/"$V".asc
    test -f $P || continue
    python plot_map.py --file=$P
    test -x open && open $V.png
done
