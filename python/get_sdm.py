# This script obtains geoTiff data from zenodo and converts it
# to ESRI ASCII .asc files
#
# SPDX-FileCopyrightText: 2024 Helmholtz-Zentrum hereon GmbH
# SPDX-License-Identifier: CC0-1.0
# SPDX-FileContributor: Carsten Lemmen <carsten.lemmen@hereon.de>
#
# Data source is
#

import pooch
import rasterio
import tempfile
import pathlib
import argparse
import re
import numpy as np

citation = "Probst, W. N. (2024). Modelled distributions of fish and epibenthic invertebrates in the southern North Sea [Data set]. Thuenen-Institute of Sea Fisheries."
doi = "10.5281/zenodo.11261320"


def round_log(M: np.ndarray) -> np.ndarray:
    log_max = int(np.ceil(np.log10(np.percentile(M[np.isfinite(M)], 90))))
    return np.round(M, 3 - log_max)


def get_args():
    parser = argparse.ArgumentParser(
        description="Convert GeoTIFF bands to ESRI ASCII files."
    )
    parser.add_argument(
        "filename",
        type=str,
        help="GeoTIFF filename to process",
        nargs="?",  # Makes it optional
        default="bio.fc.2030.2040.tiff",  # Default value if not provided
    )
    parser.add_argument(
        "species",
        type=list,
        help="Species information to extract",
        nargs="?",  # Makes it optional
        default=["sprattus", "merlangius", "solea", "platessa", "crangon", "nephrops"],
    )
    return parser.parse_args()


def save_asc(band_data, output_path, profile):
    profile.update(
        driver="AAIGrid", count=1, nodata=-9
    )  # Ensure only 1 band is written
    band_data = round_log(band_data)
    band_data[np.isnan(band_data)] = -9

    with rasterio.open(
        output_path.with_name(output_path.stem + ".asc"), "w", **profile
    ) as dst:
        dst.write(band_data, 1)

    print(f"File saved as: {output_path.with_name(output_path.stem + '.asc')}")


def fetch_geotiff(doi: str, filename: str) -> pathlib.Path:
    url = f"https://zenodo.org/record/{doi.split('.')[-1]}/files/{filename}"

    if filename == "bio.fc.2030.2040.tiff":
        known_hash = "51f60cd8d9da042e3405dd9941009d75160385c08d249187ce1a36109aa23024"
    else:
        known_hash = None

    pooch.retrieve(
        url=url,
        fname=filename,
        path=tempfile.gettempdir(),
        known_hash=known_hash,
    )
    return pathlib.Path(tempfile.gettempdir()) / filename


def read_geotiff(p: pathlib.Path):
    dataset = rasterio.open(p)
    print(f"GeoTiff has {dataset.count} bands.")
    return dataset


def convert_band_to_asc(dataset, band_num: int, output_path: pathlib.Path | str):
    band_data = dataset.read(band_num)  # Read only one band at a time
    profile = dataset.profile
    profile.update(dtype=rasterio.float32, compress="lzw")  # Optional updates

    # Get the band name (description) if available, else use band number
    band_name = (
        dataset.descriptions[band_num - 1]
        if dataset.descriptions[band_num - 1]
        else f"band{band_num}"
    ).split(".")[0]

    # Clean the band name for use in filenames (optional, if necessary)
    band_name_clean = band_name.replace(" ", "_").replace("/", "_")

    print(output_path)

    # Save each band as a separate .asc file, using band name in the filename
    save_asc(
        band_data,
        pathlib.Path(str(output_path) + f"{band_name_clean}"),
        profile,
    )


def main():
    args = get_args()
    geotiff_path = fetch_geotiff(doi, args.filename)
    dataset = read_geotiff(geotiff_path)
    years = re.findall(r"\b\d{4}\b", args.filename)[-2:]
    years = np.mean(list(map(int, years))).astype(int)

    for band_num in range(1, dataset.count + 1):
        band_name = dataset.descriptions[band_num - 1]
        for s in args.species:
            if s not in band_name:
                continue
            output_path = pathlib.Path(__file__).parent / f"../data/thuenen/{years}_"

        if not any([s in band_name for s in args.species]):
            continue
        convert_band_to_asc(dataset, band_num, output_path)


if __name__ == "__main__":
    main()
