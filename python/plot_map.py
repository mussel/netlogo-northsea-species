import argparse
import numpy as np
import matplotlib.pyplot as plt
import matplotlib.ticker as mticker
import cartopy.crs as ccrs
import rasterio
import cmocean.cm as cmo
import cartopy.feature as cfeature
from cartopy.mpl.gridliner import LONGITUDE_FORMATTER, LATITUDE_FORMATTER
from matplotlib.colors import ListedColormap
from matplotlib.offsetbox import AnchoredText
import numpy.ma as ma
import pathlib
import yaml
import re
import seaborn as sns
from matplotlib.colors import Normalize


# Load the dictionary of variables
with open("variables.yaml", "r") as file:
    variables = yaml.safe_load(file)["variables"]


def read_asc(asc_file):
    with rasterio.open(asc_file) as src:
        # Display the raster on the existing map
        raster_data = src.read(1)  # Assuming a single-band raster
        transform = src.transform

        mask = (raster_data == src.nodata) & (raster_data == -9999)
        masked_raster_data = ma.masked_where(mask, raster_data).astype(float)

        masked_raster_data = ma.masked_where(raster_data == -9999, masked_raster_data)

        # Create an extent based on the raster's shape
        extent = [
            transform[2],
            transform[2] + transform[0] * src.width,
            transform[5] + transform[4] * src.height,
            transform[5],
        ]

    lon = np.linspace(extent[0], extent[1], masked_raster_data.shape[1])
    lat = np.linspace(extent[2], extent[3], masked_raster_data.shape[0])
    lat = np.flip(lat)

    return masked_raster_data, extent, lon, lat


def BluesGreens():
    # Create colormap for negative values (Blues) and positive values (Greens)
    blues = plt.cm.Blues_r(np.linspace(0, 1, 128))
    greens = plt.cm.Greens(np.linspace(0, 1, 128))
    bluesgreens = np.vstack((blues, greens))
    return ListedColormap(bluesgreens, name="BluesGreens")


def add_grid(ax):
    gl = ax.gridlines(
        crs=ccrs.PlateCarree(),
        draw_labels=True,
        linewidth=1,
        color="lightgray",
        alpha=0.3,
        linestyle="--",
        x_inline=False,
        y_inline=False,
    )
    gl.top_labels = False
    gl.right_labels = False
    gl.xlocator = mticker.FixedLocator([6, 7, 8])
    gl.ylocator = mticker.FixedLocator([53, 54, 55])
    gl.xformatter = LONGITUDE_FORMATTER
    gl.yformatter = LATITUDE_FORMATTER
    label_style = {"size": 9, "color": "lightgray", "weight": "normal"}
    gl.xlabel_style = label_style
    gl.ylabel_style = label_style


def get_meta(file: pathlib.Path | str):
    meta = {
        "min": None,
        "max": None,
        "unit": "",
        "cmap": "viridis",
        "title": None,
        "valid_range": [-np.inf, np.inf],
        "discrete": False,
    }
    for key in variables.keys():
        if key not in pathlib.Path(file).stem:
            continue

        # Default title is the file name's stem
        meta["title"] = key
        for subkey in meta.keys():
            meta[subkey] = variables[key][subkey] if subkey in variables[key] else None
        break

    print(meta)
    return meta


def plot_bathymetry(args: dict):
    topo, extent, lon, lat = read_asc(args.bathymetry)

    depth = -ma.masked_where(topo >= 0, topo)
    height = ma.masked_where(topo < 0, topo)

    depth_levels = np.asarray([0, 2, 3, 5, 10, 20, 30, 40, 80])
    height_levels = np.asarray([0, 5, 10, 50, 100])
    # topo_levels = np.unique(np.hstack([-depth_levels, depth_levels]))

    # Plot
    fig = plt.figure(figsize=(10, 6))
    ax = plt.axes(projection=ccrs.PlateCarree())
    ax.set_extent(args.extent)
    ax.set_aspect("auto")

    # Add shaded relief for topography
    ax.add_feature(
        cfeature.NaturalEarthFeature(
            "physical",
            "land",
            "10m",
            edgecolor="face",
            facecolor=cfeature.COLORS["land"],
            zorder=0,
        )
    )
    ax.add_feature(
        cfeature.NaturalEarthFeature(
            "physical",
            "ocean",
            "10m",
            edgecolor="none",
            facecolor=cfeature.COLORS["water"],
            zorder=0,
        )
    )
    # ax.coastlines(resolution='10m', color='black', zorder=1)

    plt.contourf(
        lon,
        lat,
        height,
        cmap="Greens",
        levels=height_levels,
        alpha=0.2,
        extend="max",
        origin="upper",
    )

    plt.contourf(
        lon,
        lat,
        depth,
        cmap="Blues",
        levels=depth_levels,
        extend="max",
        origin="upper",
        alpha=0.2,
    )

    file = pathlib.Path(args.file)

    data, extent, lon, lat = read_asc(file)
    meta = get_meta(file)
    if meta["min"] is None:
        meta["min"] = np.min(data)
    if meta["max"] is None:
        meta["max"] = np.max(data)

    data = ma.masked_where((data < meta["min"]) | (data > meta["max"]), data)
    # data = ma.masked_where(data < meta["valid_range"][0], data)
    # data = ma.masked_where(data > meta["valid_range"][1], data)

    if meta["discrete"]:
        # nunique = np.unique(data)
        cmap = meta["cmap"]
    else:
        cmap = meta["cmap"]

    data_hdl = plt.pcolormesh(
        lon,
        lat,
        data,
        alpha=0.7,
        cmap=cmap,
    )

    # plt.contourf(topo, extent=extent, cmap=BluesGreens(), levels=topo_levels, extend='max', origin='upper')

    # Add colorbars
    # cbarocean = plt.colorbar(topo_hdl, label="Depth (m)", orientation="vertical", shrink=0.9)
    # cbarocean.ax.invert_yaxis()

    title_box = AnchoredText(
        f'{meta["title"]}',
        loc="upper left",
        frameon=True,
        pad=0.5,
        prop=dict(size=15, weight="bold"),
    )
    title_box.patch.set_boxstyle("round,pad=0.2")
    title_box.patch.set_alpha(0.7)  # Make it slightly transparent
    ax.add_artist(title_box)

    cbardata = fig.add_axes([0.92, 0.11, 0.05, 0.75])
    cbardata = plt.colorbar(data_hdl, orientation="vertical", cax=cbardata)
    cbardata.ax.yaxis.set_ticks_position("right")  # Keep the ticks on the right side
    cbardata.ax.set_title(f'{to_latex(meta["unit"])}', fontsize=12)

    if meta["discrete"]:
        add_bar(cbardata, data)
    else:
        add_violin(cbardata, data)

    return fig, ax


def to_latex(string):
    return re.sub(r"(\w+)-(\d+)", r"\1$^{-\2}$", string)


def add_bar(cbar, data):
    ax = cbar.ax.twiny()
    if data.ndim > 1:
        data = data.flatten()

    sns.histplot(
        bins=np.unique(data),
        y=data,
        ax=ax,
        color="black",
        discrete=True,
    )  # , palette=colors)

    pos = ax.get_position()
    ax.set_position([pos.x0 + 0.01, pos.y0, pos.width, pos.height])
    ax.grid(False)  # Turn off grid if it's not needed


def add_violin(cbar, data):
    ax = cbar.ax.twiny()
    # cmap = cbar.cmap
    data = data.flatten()

    # norm = Normalize(vmin=0, vmax=len(data) - 1)
    # colors = [cmap(norm(i)) for i in range(len(data))]

    sns.violinplot(
        y=data,
        ax=ax,
        orient="v",
        fill=False,
        inner="quartile",
        split=True,
        cut=0,
        color="black",
    )  # , palette=colors)

    pos = ax.get_position()
    ax.set_position([pos.x0 + 0.01, pos.y0, pos.width, pos.height])
    ax.grid(False)  # Turn off grid if it's not needed


def main():
    parser = argparse.ArgumentParser(
        description="Plot raster map with bathymetry using Cartopy."
    )
    parser.add_argument(
        "--file",
        "-f",
        type=str,
        help="Path to raster file (e.g., shrimp.asc)",
        default="../netlogo/results/effort_h_0016_20221007-0000.asc",
    )
    parser.add_argument(
        "--bathymetry",
        "-b",
        type=str,
        help="Path to bathymetry file (e.g., bathymetry.asc)",
        default="../data/gebco/GEBCOSubIceTopo_e-2_w10_s53_n56.asc",
    )
    parser.add_argument(
        "--extent",
        "-t",
        help="List of lat/lon limits in order [lonmin, lonmax, latmin, latmax]",
        default=[5, 9.1, 53.2, 55.6],
    )
    parser.add_argument(
        "--fontsize",
        "-s",
        help="Base font size for plotting",
        default=12,
    )
    args = parser.parse_args()

    fig, ax = plot_bathymetry(args)
    add_grid(ax)
    # plt.subplots_adjust(left=0.01, right=0.99, top=0.99, bottom=0.01)
    plt.savefig(pathlib.Path(args.file).stem + ".png", bbox_inches="tight", dpi=300)
    plt.savefig(pathlib.Path(args.file).stem + ".pdf")


if __name__ == "__main__":
    main()
