# GitLab CI configuration file for building the JOSS paper
#
# SPDX-FileCopyrightText: 2022-2024 Helmholtz-Zentrum hereon GmbH
# SPDX-License-Identifier: CC0-1.0
# SPDX-FileContributor Carsten Lemmen <carsten.lemmen@hereon.de

# This job generates the JOSS pdf paper and CITATION.cff
# using our self-hosted pandoc docker.  We prefer the
# artifacts created by the joss-inara job below
joss-paper:
  image: $CI_REGISTRY_IMAGE/pandoc:3.1.6.2
  script:
    - joss -p -o pdf,cff doc/joss/src/paper.md
  artifacts:
    paths:
  needs: []
  stage: deploy
  rules:
    - if: '$CI_COMMIT_BRANCH == "docker"'

# This job generates the JOSS pdf paper and CITATION.cff
# using our self-hosted joss docker, but needs very long
# and is not run by default.
joss-docker:
  image: docker
  tags: ["docker", "dind"]
  services:
    - docker:dind
  variables:
    CONTAINER_REGISTRY_IMAGE: $CI_REGISTRY_IMAGE/pandoc:3.1.6.2
  allow_failure: false
  interruptible: true
  stage: deploy

  before_script:
    - docker login -u $CI_REGISTRY_USER -p $CI_JOB_TOKEN $CI_REGISTRY
  script:
    - docker pull ${CONTAINER_REGISTRY_IMAGE}
    - docker run -v $(pwd):/data ${CONTAINER_REGISTRY_IMAGE} joss /data/doc/joss/src/paper.md
  needs: []
  rules:
    - if: '$CI_COMMIT_BRANCH == "docker"'
  artifacts:
    paths:

# The JOSS-provided workflow did not work for issues between inara docker
# and gitlab, see https://github.com/openjournals/inara/issues/34.
# This is resolved by resetting the container's entrypoint.
joss-inara:
  stage: deploy
  image:
    name: openjournals/inara
    entrypoint: ["/bin/sh", "-c"]
  variables:
    GIT_SHA: $CI_COMMIT_SHA
    JOURNAL: joss
  script:
    - inara -o pdf,cff ./doc/joss/paper.md
  allow_failure: false
  artifacts:
    paths:
  needs: []
