<!--
SPDX-FileContributor: Carsten Lemmen <carsten.lemmen@hereon.de>
SPDX-FileCopyrightText: 2023-2024 Helmholtz-Zentrum hereon GmbH
SPDX-License-Identifier: CC0-1.0
-->

This directory contains supplementary data for running the NetLogo model. All data is available as open data under the Creative Commons Attribution 4.0 International license.

Probst, W. N. (2024). Modelled distributions of fish and epibenthic invertebrates in the southern North Sea [Data set]. Thuenen-Institute of Sea Fisheries. https://doi.org/10.5281/zenodo.11261320

To re-download the data from the Zenodo repository, please use the
provided script `../../python/get_sdm.py`. This converts the GeoTiff to ESRII ASCII format, cleans the data and reduces the precision.

```
python ../../python/get_sdm.py bio.fc.2030.2040.tiff
python ../../python/get_sdm.py bio.sdm.2014.2023.tiff
```

Species distribution maps were produced for present state ("2018", representative of decade 2014-2023) and for a future state ("2035", 2030-2040); these are annual mean maps. The unit is kg km-2 for all species.

## More available data

On the Zenodo repository, more data is available:

```
bio.emp.2004.2012
bio.fc.2030.2040
bio.fc.2060.2070
bio.fc.2090.2100
bio.sdm.2004.2012
poc.emp.2004.2012
poc.fc.2030.2040
poc.fc.2060.2070
poc.fc.2090.2100
poc.sdm.2004.2012
```
