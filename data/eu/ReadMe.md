<!--
SPDX-FileContributor: Carsten Lemmen <carsten.lemmen@hereon.de>
SPDX-FileCopyrightText: 2024 Helmholtz-Zentrum hereon GmbH
SPDX-License-Identifier: CC0-1.0
-->

This folder contains EU legislative data, based on

1. Consolidated text: Commission Delegated Regulation (EU) 2017/118 of 5 September 2016 establishing fisheries conservation measures for the protection of the marine environment in the North Sea https://eur-lex.europa.eu/legal-content/EN/TXT/?uri=CELEX:02017R0118-20230809
