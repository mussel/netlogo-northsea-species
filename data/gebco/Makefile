# SPDX-FileCopyrightText: 2024 Helmholtz-Zentrum hereon GmbH
# SPDX-License-Identifier: CC0-1.0
# SPDX-FileContributor: Carsten Lemmen <carsten.lemmen@hereon.de>

# To download data, you need an access token (after free registration on opentopography.org)

# Available datasets from OpenTopography
# SRTMGL3 (SRTM GL3 90m)
# SRTMGL1 (SRTM GL1 30m)
# SRTMGL1_E (SRTM GL1 Ellipsoidal 30m)
# AW3D30 (ALOS World 3D 30m)
# AW3D30_E (ALOS World 3D Ellipsoidal, 30m)
# SRTM15Plus (Global Bathymetry SRTM15+ V2.1 500m)
# NASADEM (NASADEM Global DEM)
# COP30 (Copernicus Global DSM 30m)
# COP90 (Copernicus Global DSM 90m)
# EU_DTM (DTM 30m)
# GEDI_L3 (DTM 1000m)
# GEBCOIceTopo (Global Bathymetry 500m)
# GEBCOSubIceTopo (Global Bathymetry 500m)

DATE=$(shell  date +%Y-%m-%d)
PWD=$(shell pwd)
LATMIN=53
LATMAX=56
LONMIN=-2
LONMAX=10
APIKEY=$(OPENTOPOGRAPHY_TOKEN)
DATASET=GEBCOSubIceTopo
URL="https://portal.opentopography.org/API/globaldem?demtype=$(DATASET)&south=$(LATMIN)&north=$(LATMAX)&west=$(LONMIN)&east=$(LONMAX)&outputFormat=AAIGrid&API_Key=$(APIKEY)"
FILENAME=$(DATASET)_e$(LONMIN)_w$(LONMAX)_s$(LATMIN)_n$(LATMAX).asc

.PHONY: default clean

default: download

download: $(FILENAME)

$(FILENAME):
	echo curl --progress-bar -X 'GET' $(URL)  -H 'accept: */*'  -o $(FILENAME)

clean:
	@rm -f $(DATASET)_*.asc
