<!--
SPDX-FileContributor: Carsten Lemmen <carsten.lemmen@hereon.de>
SPDX-FileCopyrightText: 2023-2024 Helmholtz-Zentrum hereon GmbH
SPDX-License-Identifier: CC0-1.0
-->

This directory contains supplementary data for running the NetLogo model. Here, the
bathymetry data from GEBCO is saved.

## Obtaining the data

1. Go to [https://download.gebco.net](https://download.gebco.net)
2. nter the domain boundaries n56.0_s53.0_w2.0_e10.0, select in the Grid section `2D netCDF` and `Esri ASCII`
3. Scroll down and `Add to basket`, `Go to basket` and `Download data`
4. Unzip the downloaded file and copy the files `gebco_2023_n56.0_s53.0_w2.0_e10.0.{nc,asc,prj}` to here.

## Data set attribution for GEBCO_2023 Grid

If the data sets are used in a presentation or publication then we ask that you acknowledge the source.This should be of the form:

GEBCO Compilation Group (2023) GEBCO 2023 Grid ([doi:10.5285/f98b053b-0cbc-6c23-e053-6c86abc0af7b](https://doi.org/10.5285/f98b053b-0cbc-6c23-e053-6c86abc0af7b))
