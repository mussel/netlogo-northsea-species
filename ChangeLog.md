<!--
SPDX-FileCopyrightText: 2023-2024 Helmholtz-Zentrum hereon GmbH
SPDX-License-Identifier: CC0-1.0
SPDX-FileContributor: Carsten Lemmen <carsten.lemmen@hereon.de>
-->

# Change log and release notes

The format is based on [Keep a Changelog](https://keepachangelog.com/en/1.1.0/),
and this project adheres to [Semantic Versioning](https://semver.org/spec/v2.0.0.html).

# Release notes for version 1.4.0 - in development

Version 1.4.0 is a feature release of our Viable North Sea (ViNoS) software.

- Generalised handling of species
- Title change from Small-scale to Coastal fishery
- Published paper on Good Modeling Software Practise

# Release notes for version 1.3.3 - Jun 8, 2024

Version 1.3.3 is a maintenance release of our Viable North Sea (ViNoS) software.

- JOSS paper is published with DOI 10.21105.joss.05731
- Progress draft of closure experiments (in branch `ices`)
- New draft on modeling software practise
- Added python controlling API and binder (in branch `pynetlogo`)

# Release notes for version 1.3.2 - Mar 8, 2024

Version 1.3.2 is a maintenance release of our Viable North Sea (ViNoS) software.

- added BehaviorSpace experiment for closures
- added BehaviorSpace experiments for prices
- added Trilateral Ecotopes of the Wadden Sea tidal basins
- new closure experiments draft
- CI pipeline enhancements

# Release notes for version 1.3.1 - Feb 16, 2024

Version 1.3.1 is a maintenance release of our Viable North Sea (ViNoS) software.

- new assumptions document draft
- frequency of exporting patch data is now a chooser
- added dynamic oil price 1986-2023
- add random component to catch

# Release notes for version 1.3.0 - Dec 5, 2023

Version 1.3.0 is a feature release of our Viable North Sea (ViNoS) software. The main purpose of this release are benchmarks and realism improvements, as well as a new storymap and individual logbooks.

- added storymap
- added individual fisher's logbook
- added monthly landing expectation
- added monthly reporting
- added benchmark monitors

# Release notes for version 1.2.3 - Oct 20, 2023

Version 1.2.3 is a maintenance release of our Viable North Sea (ViNoS) software. The main purpose of this release are further improvements to the user interface and trip optimization.

- speed improvements
- include Global Fishery Watch effort data
- don't haul in traffic
- output scenarios for 2020, 2030 and for tides
- display of map values and nicer legend
- better python visualization of maps

# Release notes for version 1.2.2 - Oct 11, 2023

Version 1.2.2 is a maintenance release of our Viable North Sea (ViNoS) software. The
main purpose of this release are improvements to the user interface.

- consistent naming of species (now changed to English short form)
- added action (memory) histogram and spatial visualization
- added traffic hindering fishing activity
- fixed correct phasing of tides
- fixed creep-fill for species

# Release notes for version 1.2.1 - Sep 27, 2023

Version 1.2.1 is a maintenance release of our Viable North Sea (ViNoS) software. The
main purpose of this release are improvements to functionality of the trip
optimization.

- added tidal signal for patch accessibility
- fill entire domain with species biomass data
- revisit location based on estimated gain
- tabular output of totals for analysis of LPUE and fuel intensity
- updated OWF data and historic timeline of extent

# Release notes for version 1.2.0 - Sep 6, 2023

Version 1.2.0 is a feature release of our Viable North Sea (ViNoS) software. The
main purpose of this release are improvements to functionality of the trip
optimization.

- Keep gear fixed until it is explicitly changed, despite changing priorities
- Keep a record of `memory-size` for revisiting successful catch locations
- Cleanup towards Clean Code
- UI clarifications in legends and labels
- Reducing diagnostic output for production runs
- Rest during bad weather and Ramadan (shrimpers)

# Release notes for version 1.1.1 - Aug 8, 2023

This is a re-release of v1.1.0 to fix errors with Zenodo depositioning.

- added zenodo metadata

# Release notes for version 1.1.0 - May 25, 2023

Version 1.1.0 is a maintenance release of our Viable North Sea (ViNoS) software. The main purpose of this release is stabilize the software and the submission of the ODD.

- Added collaborators
- Published in Helmholtz Research Directory and on Zenodo
- verified VIABLE approch in model
- Completed ODD

# Release notes for version 1.0.0 - April 17, 2023

Version 1.0.0 is the initial release of our Viable North Sea (ViNoS) software. The main purpose of this release is to open the research software to the public. This release features

- NetLogo 6.2 or NetLogo 6.3 as a development platform
- basic functionality of all agents and their interactions
- preliminary results generated and interpretable
- completed software infrastructure including license management and continuous integration (CI)
- submission of the software to the Journal of Open Source Software (JOSS)

Issues to look out for in the next version are stabilization of the software, a submission of the ODD protocol to COMSES.net, and more elaborate work on the fishery economics.
