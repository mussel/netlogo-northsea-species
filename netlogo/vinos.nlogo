; SPDX-FileCopyrightText: 2024 Helmholtz-Zentrum hereon GmbH
; SPDX-License-Identifier: Apache-2.0
; SPDX-FileContributor: Carsten Lemmen <carsten.lemmen@hereon.de>
;
; All functionality is contained in subsidiary NetLogo source code
; files (.nls) in the ./include folder
;
__includes [
  "include/main.nls"            ; global functionality, setup and go
  "include/prey.nls"            ; Prey breed and methods
  "include/port.nls"            ; Port breed and methods
  "include/gear.nls"            ; Gear breed and methods
  "include/action.nls"          ; Action breed and methods
  "include/boat.nls"            ; Boat gear and methods
  "include/geodata.nls"         ; GIS functionality
  "include/calendar.nls"        ; Time and calendar procedures
  "include/plot.nls"            ; Plotting functionality
  "include/utilities.nls"       ; Utility functions
  "include/scene.nls"           ; Zooming and view functionality
  "include/time-series.nls"     ; Time series extension
  "include/python.nls"          ; Python extension
  "include/ci.nls"              ; Continuous integration
  "include/video.nls"           ; Video extension
  "include/output.nls"          ; Output handling
]
@#$#@#$#@
GRAPHICS-WINDOW
262
59
1233
429
-1
-1
3.01
1
10
1
1
1
0
0
0
1
0
319
0
119
1
1
1
days
30.0

BUTTON
13
89
79
122
NIL
setup
NIL
1
T
OBSERVER
NIL
S
NIL
NIL
1

CHOOSER
11
177
159
222
scene
scene
"Shrimp" "Plaice" "Sole" "Effort h" "Effort MWh" "SAR" "Bathymetry" "Accessibility" "OWF" "Plaicebox" "Area" "Shore proximity" "Port proximity" "Depth" "Tide" "Action" "Traffic" "Catch" "GFW effort" "EMODnet effort"
6

BUTTON
83
90
152
123
go
go
T
1
T
OBSERVER
NIL
G
NIL
NIL
0

BUTTON
160
177
215
221
update
update-scene
NIL
1
T
OBSERVER
NIL
U
NIL
NIL
1

SWITCH
1244
123
1364
156
show-ports?
show-ports?
1
1
-1000

SLIDER
12
350
251
383
adaptation
adaptation
0
1
0.779
0.001
1
NIL
HORIZONTAL

SLIDER
12
309
253
342
memory-size
memory-size
0
50
50.0
1
1
NIL
HORIZONTAL

SLIDER
11
421
183
454
diesel-price
diesel-price
0
100
0.0
5
1
ct l-1
HORIZONTAL

TEXTBOX
15
10
1034
63
Viable North Sea Agent-based Model of German Coastal Fisheries  (ViNoS v1.4.0)
22
2.0
1

TEXTBOX
15
53
239
96
To operate the model, wait until the map is loaded and then click on \"go\".
11
0.0
1

TEXTBOX
13
143
219
185
Change the information for the basemap here and hit `update`
11
0.0
1

PLOT
332
595
719
745
catch
days
catch/kg
0.0
10.0
0.0
10.0
true
true
"plot-setup-catch-by-gear" "plot-update-catch-by-gear"
PENS

PLOT
725
500
1003
620
gain-by-gear
days
gain/k€
0.0
10.0
0.0
10.0
true
false
"plot-setup-gain-by-gear" "plot-update-gain-by-gear"
PENS

PLOT
726
626
1003
746
priority-by-gear
NIL
NIL
0.0
10.0
0.0
10.0
true
false
"plot-setup-priority-by-gear" "plot-update-priority-by-gear"
PENS

TEXTBOX
1247
57
1362
113
Additional \nforeground\ninformation, \nhit `update`
11
0.0
1

SWITCH
1244
199
1364
232
owf?
owf?
1
1
-1000

SWITCH
1243
241
1365
274
box?
box?
1
1
-1000

SWITCH
1242
282
1365
315
sar?
sar?
1
1
-1000

BUTTON
1240
320
1366
353
update-background
update-drawings
NIL
1
T
OBSERVER
NIL
NIL
NIL
NIL
1

TEXTBOX
15
390
260
417
Diesel for ships is tax-exempt and  costs on average 0.5 € l-1. If zero, then history used.
10
0.0
1

TEXTBOX
12
462
162
490
Staff costs are usually around 80 € h-1
11
0.0
1

SLIDER
10
492
182
525
wage
wage
50
120
50.0
5
1
€ h-1
HORIZONTAL

PLOT
9
595
327
745
boat-property
NIL
# boats
0.0
6.0
0.0
10.0
true
true
"plot-setup-boat-property" "plot-update-boat-property"
PENS

CHOOSER
9
541
164
586
boat-property-chooser
boat-property-chooser
"distance-at-sea" "capacity" "catch-efficiency" "gear" "engine" "capital" "length" "max-distance" "max-duration" "operating-costs" "prey" "steaming-speed" "time-at-sea" "time-at-sea-left" "fuel-cost" "trip-phase" "type" "boat-total-landings" "boat-total-fuel-consumption" "boat-total-days-at-sea" "effort h"
0

SWITCH
1244
160
1365
193
show-boats?
show-boats?
1
1
-1000

BUTTON
167
542
239
586
update-plot
plot-update-boat-property
NIL
1
T
OBSERVER
NIL
NIL
NIL
NIL
0

BUTTON
1241
358
1368
391
clear
clear-drawing
NIL
1
T
OBSERVER
NIL
C
NIL
NIL
1

SWITCH
156
91
246
124
one?
one?
1
1
-1000

SLIDER
12
271
252
304
time-offset
time-offset
-200
200
-24.0
1
1
months from now
HORIZONTAL

TEXTBOX
600
655
674
695
CSH = shrimp\nPLE = plaice\nSOL = sole\nTBB = beam trawl\nOTB = otter trawl
6
0.0
1

PLOT
1008
665
1236
785
action
NIL
# action
0.0
10.0
0.0
10.0
true
true
"plot-setup-action-histogram" "plot-update-action-histogram"
PENS

CHOOSER
1008
615
1159
660
action-chooser
action-chooser
"gain" "catch" "gear" "depth" "coast" "age"
0

BUTTON
1163
614
1237
660
update
plot-update-action-histogram
NIL
1
T
OBSERVER
NIL
NIL
NIL
NIL
1

SWITCH
11
226
159
259
show-values?
show-values?
1
1
-1000

CHOOSER
1005
439
1154
484
monthly-chooser
monthly-chooser
"Fleet land. t" "Fleet revenue k€" "Fleet effort h" "Fleet seadays" "Effort MWh" "Effort h"
2

PLOT
1008
489
1236
609
Monthly statistics
Month
NIL
1.0
12.0
0.0
10.0
false
false
"plot-setup-month-histogram" "plot-update-month-histogram"
PENS

BUTTON
1160
440
1234
484
update
plot-update-month-histogram
NIL
1
T
OBSERVER
NIL
NIL
NIL
NIL
0

OUTPUT
247
501
718
580
10

TEXTBOX
247
463
397
491
Shrimper fleet aggregated data (per vessel and year)
10
0.0
1

CHOOSER
864
439
1002
484
output-frequency
output-frequency
"none" "daily" "weekly" "monthly" "yearly"
0

CHOOSER
700
439
856
484
closure-scenario
closure-scenario
"none" "BAU" "WSHA 100%" "WSHA 30% cells" "WSHA 30% basins"
3

@#$#@#$#@
# Viable North Sea (ViNoS) Agent-based Model of German Coastal Fisheries

Viable North Sea (ViNoS), an Agent-based model (ABM) of the German coastal fisheries is a Social-Ecological Systems (SES) model focussing on the adaptive behaviour of fishers facing regulatory, economic, and resource changes.  Coastal fisheries are an important part both of the cultural perception of the German North Sea coast and of its fishing industry. These fisheries are typically family-run operations that use smaller boats and traditional fishing methods to catch a variety of bottom-dwelling species, including plaice, sole, brown shrimp.

Fisheries in the North Sea face area competition with other uses of the sea -- long practiced ones like shipping, gas exploration and sand extractions, and currently increasing ones like marine protection and offshore wind farming (OWF).  German authorities have just released a new maritime spatial plan implementing the need for 30% of protection areas demanded by the United Nations High Seas Treaty and aiming at up to 70 GW of offshore wind power generation by 2045.   Fisheries in the North Sea also have to adjust to the northward migration of their established resources following the climate heating of the water.  And they have to re-evaluate their economic balance by figuring in the foreseeable rise in oil price and the need for re-investing into their aged fleet.

## Purpose, scope and audience
The purpose of this ABM is to provide an interactive simulation environment that describes spatial, temporal and structural adaptations of the fleet.  It adaptively describes

 * where to fish  and how far to go out to sea
 * how often to go out
 * what gear to use and what species to target

Its scope is the German North sea coastal fisheries.  This encompasses some 300 vessels based in German ports along the North Sea coast and fishing in the German Bight, including but not restricted to Germany's exclusive economic zone (EEZ). The target species is currently restricted to the most important ones: plaice, sole and brown shrimp, but is in principle extensible to further target species like Norwegian lobster or whiting.

The intended audience of the ABM are marine researchers and government agencies concerned with spatial planning, environmental status assessment, and climate change mitigation.  It can also assist in a stakeholder dialogue with tourism and fishers to contextualize the complexity of the interactions between fisheries economics, changing resources and regulatory restrictions.  It is intended to be used for scenario development for future sustainable fisheries at the German North Sea coast.

## Agents and implementation features

Agents are boats,  the gear they use, the strategies they employ, and their prey.  All agents are encapsulated in object-oriented design as `breeds`.  The agents' methods implement the interaction rules between agents and between agents and their environment.  Key interactions are the movement rules of boats across the seascape, the harvesting of resources, and the cost-benefit analysis of a successful catch and its associated costs.  Adaptation occurs at the level of preference changes for gear selection (and prey species), and the time and distance preferences for fishing trips.

A notable programming feature is the integration of the legend with the (map) `view`, a feature that is lacking from the default capabilities of NetLogo.  There have been discussions on how to implement a legend using the `plot` element[1], but so far this is the only NetLogo model known to the authors implementing a legend with the `view`.

## Model documentation and license

Please cite this model as

Lemmen, Carsten, Sascha Hokamp, Serra Örey & Jürgen Scheffran (2024). Viable North Sea (ViNoS): A NetLogo Agent-based Model of German Coastal Fisheries. Journal of Open Source Software, 9(95), 5731, [https://doi.org/10.21105/joss.05731](https://doi.org/10.21105/joss.05731)

The model is documented  with the Overview, Design, and Details (ODD, [2]) standard protocol for ABMs, available in the repository as [../doc/odd/odd.md](../doc/odd/odd.md)
All data from third parties is licensed under various open source licenses.  The model, its results and own proprietary data was released under open source licenses, mostly Apache 2.0 and CC-BY-SA-4.0.  A comprehensive documentation of all is provided via REUSE [3].

### Acknowledgements

We acknowledge contributions from W. Nikolaus Probst, Seong Jieun, Verena Mühlberger and Kai Wirtz for providing data, fruitful discussions and contributing to the ODD document. We thank all members of the MuSSeL consortium making this software relevant in a research context.  The development of the model was made possible by the grants 03F0862A, 03F0862C, 03F0862D, 03F0862E  "Multiple Stressors on North Sea Life" [4] within the 3rd Küstenforschung Nord-Ostsee (KüNO) call of the Forschung für Nachhaltigkeit (FONA) program of the Germany Bundesministerium für Bildung und Forschung (BMBF).

### License

Authors Carsten Lemmen, Sascha Hokamp, Serra Örey
Copyright 2022-2024 Helmholtz-Zentrum hereon GmbH, Universität Hamburg, Hochschule Bremerhaven

Licensed under the **Apache License, Version 2.0** (the "License"); you may not use this file except in compliance with the License. You may obtain a copy of the License at http://www.apache.org/licenses/LICENSE-2.0.

Unless required by applicable law or agreed to in writing, software distributed under the License is distributed on an "AS IS" BASIS, WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied. See the License for the specific language governing permissions and limitations under the License.

![Hereon](../assets/logo-hereon.png)       ![UHH](../assets/logo-uhh.png)     ![HSB](../assets/logo-hsb.png)

## References

[1]: Arn, Luke C, Javier Sandoval. 2018. “Netlogo How to Add a Legend?” Stackoverflow. 2018. https://stackoverflow.com/questions/51328633/netlo go-how-to-add-a-legend.

[2]: Grimm, Volker, Steven F. Railsback, Christian E. Vincenot, Uta Berger, Cara Gallagher, Donald L. Deangelis, Bruce Edmonds, et al. 2020. “The ODD protocol for describing agent-based and other simulation models: A second update to improve clarity, replication, and structural realism.” JASSS 23 (2). https://doi.org/10.18564/jasss.4259.

[3]: “REUSE Software.” 2023. Free Software Foundation Europe e.V. 2023. https://reuse.software.

[4]: "Multiple Stressors on North Sea Life.  https://www.mussel-project.de

![Funded by BMBF](../assets/logo-bmbf-funded.png)
@#$#@#$#@
default
true
0
Polygon -7500403 true true 150 5 40 250 150 205 260 250

airplane
true
0
Polygon -7500403 true true 150 0 135 15 120 60 120 105 15 165 15 195 120 180 135 240 105 270 120 285 150 270 180 285 210 270 165 240 180 180 285 195 285 165 180 105 180 60 165 15

arrow
true
0
Polygon -7500403 true true 150 0 0 150 105 150 105 293 195 293 195 150 300 150

boat top
true
0
Polygon -7500403 true true 150 1 137 18 123 46 110 87 102 150 106 208 114 258 123 286 175 287 183 258 193 209 198 150 191 87 178 46 163 17
Rectangle -16777216 false false 129 92 170 178
Rectangle -16777216 false false 120 63 180 93
Rectangle -7500403 true true 133 89 165 165
Polygon -11221820 true false 150 60 105 105 150 90 195 105
Polygon -16777216 false false 150 60 105 105 150 90 195 105
Rectangle -16777216 false false 135 178 165 262
Polygon -16777216 false false 134 262 144 286 158 286 166 262
Line -16777216 false 129 149 171 149
Line -16777216 false 166 262 188 252
Line -16777216 false 134 262 112 252
Line -16777216 false 150 2 149 62

box
false
0
Polygon -7500403 true true 150 285 285 225 285 75 150 135
Polygon -7500403 true true 150 135 15 75 150 15 285 75
Polygon -7500403 true true 15 75 15 225 150 285 150 135
Line -16777216 false 150 285 150 135
Line -16777216 false 150 135 15 75
Line -16777216 false 150 135 285 75

bug
true
0
Circle -7500403 true true 96 182 108
Circle -7500403 true true 110 127 80
Circle -7500403 true true 110 75 80
Line -7500403 true 150 100 80 30
Line -7500403 true 150 100 220 30

butterfly
true
0
Polygon -7500403 true true 150 165 209 199 225 225 225 255 195 270 165 255 150 240
Polygon -7500403 true true 150 165 89 198 75 225 75 255 105 270 135 255 150 240
Polygon -7500403 true true 139 148 100 105 55 90 25 90 10 105 10 135 25 180 40 195 85 194 139 163
Polygon -7500403 true true 162 150 200 105 245 90 275 90 290 105 290 135 275 180 260 195 215 195 162 165
Polygon -16777216 true false 150 255 135 225 120 150 135 120 150 105 165 120 180 150 165 225
Circle -16777216 true false 135 90 30
Line -16777216 false 150 105 195 60
Line -16777216 false 150 105 105 60

car
false
0
Polygon -7500403 true true 300 180 279 164 261 144 240 135 226 132 213 106 203 84 185 63 159 50 135 50 75 60 0 150 0 165 0 225 300 225 300 180
Circle -16777216 true false 180 180 90
Circle -16777216 true false 30 180 90
Polygon -16777216 true false 162 80 132 78 134 135 209 135 194 105 189 96 180 89
Circle -7500403 true true 47 195 58
Circle -7500403 true true 195 195 58

circle
false
0
Circle -7500403 true true 0 0 300

circle 2
false
0
Circle -7500403 true true 0 0 300
Circle -16777216 true false 30 30 240

cow
false
0
Polygon -7500403 true true 200 193 197 249 179 249 177 196 166 187 140 189 93 191 78 179 72 211 49 209 48 181 37 149 25 120 25 89 45 72 103 84 179 75 198 76 252 64 272 81 293 103 285 121 255 121 242 118 224 167
Polygon -7500403 true true 73 210 86 251 62 249 48 208
Polygon -7500403 true true 25 114 16 195 9 204 23 213 25 200 39 123

cylinder
false
0
Circle -7500403 true true 0 0 300

dot
false
0
Circle -7500403 true true 90 90 120

face happy
false
0
Circle -7500403 true true 8 8 285
Circle -16777216 true false 60 75 60
Circle -16777216 true false 180 75 60
Polygon -16777216 true false 150 255 90 239 62 213 47 191 67 179 90 203 109 218 150 225 192 218 210 203 227 181 251 194 236 217 212 240

face neutral
false
0
Circle -7500403 true true 8 7 285
Circle -16777216 true false 60 75 60
Circle -16777216 true false 180 75 60
Rectangle -16777216 true false 60 195 240 225

face sad
false
0
Circle -7500403 true true 8 8 285
Circle -16777216 true false 60 75 60
Circle -16777216 true false 180 75 60
Polygon -16777216 true false 150 168 90 184 62 210 47 232 67 244 90 220 109 205 150 198 192 205 210 220 227 242 251 229 236 206 212 183

fish
false
0
Polygon -1 true false 44 131 21 87 15 86 0 120 15 150 0 180 13 214 20 212 45 166
Polygon -1 true false 135 195 119 235 95 218 76 210 46 204 60 165
Polygon -1 true false 75 45 83 77 71 103 86 114 166 78 135 60
Polygon -7500403 true true 30 136 151 77 226 81 280 119 292 146 292 160 287 170 270 195 195 210 151 212 30 166
Circle -16777216 true false 215 106 30

flag
false
0
Rectangle -7500403 true true 60 15 75 300
Polygon -7500403 true true 90 150 270 90 90 30
Line -7500403 true 75 135 90 135
Line -7500403 true 75 45 90 45

flower
false
0
Polygon -10899396 true false 135 120 165 165 180 210 180 240 150 300 165 300 195 240 195 195 165 135
Circle -7500403 true true 85 132 38
Circle -7500403 true true 130 147 38
Circle -7500403 true true 192 85 38
Circle -7500403 true true 85 40 38
Circle -7500403 true true 177 40 38
Circle -7500403 true true 177 132 38
Circle -7500403 true true 70 85 38
Circle -7500403 true true 130 25 38
Circle -7500403 true true 96 51 108
Circle -16777216 true false 113 68 74
Polygon -10899396 true false 189 233 219 188 249 173 279 188 234 218
Polygon -10899396 true false 180 255 150 210 105 210 75 240 135 240

house
false
0
Rectangle -7500403 true true 45 120 255 285
Rectangle -16777216 true false 120 210 180 285
Polygon -7500403 true true 15 120 150 15 285 120
Line -16777216 false 30 120 270 120

leaf
false
0
Polygon -7500403 true true 150 210 135 195 120 210 60 210 30 195 60 180 60 165 15 135 30 120 15 105 40 104 45 90 60 90 90 105 105 120 120 120 105 60 120 60 135 30 150 15 165 30 180 60 195 60 180 120 195 120 210 105 240 90 255 90 263 104 285 105 270 120 285 135 240 165 240 180 270 195 240 210 180 210 165 195
Polygon -7500403 true true 135 195 135 240 120 255 105 255 105 285 135 285 165 240 165 195

line
true
0
Line -7500403 true 150 0 150 300

line half
true
0
Line -7500403 true 150 0 150 150

pentagon
false
0
Polygon -7500403 true true 150 15 15 120 60 285 240 285 285 120

person
false
0
Circle -7500403 true true 110 5 80
Polygon -7500403 true true 105 90 120 195 90 285 105 300 135 300 150 225 165 300 195 300 210 285 180 195 195 90
Rectangle -7500403 true true 127 79 172 94
Polygon -7500403 true true 195 90 240 150 225 180 165 105
Polygon -7500403 true true 105 90 60 150 75 180 135 105

plant
false
0
Rectangle -7500403 true true 135 90 165 300
Polygon -7500403 true true 135 255 90 210 45 195 75 255 135 285
Polygon -7500403 true true 165 255 210 210 255 195 225 255 165 285
Polygon -7500403 true true 135 180 90 135 45 120 75 180 135 210
Polygon -7500403 true true 165 180 165 210 225 180 255 120 210 135
Polygon -7500403 true true 135 105 90 60 45 45 75 105 135 135
Polygon -7500403 true true 165 105 165 135 225 105 255 45 210 60
Polygon -7500403 true true 135 90 120 45 150 15 180 45 165 90

sailboat side
false
0
Line -16777216 false 0 240 120 210
Polygon -7500403 true true 0 239 270 254 270 269 240 284 225 299 60 299 15 254
Polygon -1 true false 15 240 30 195 75 120 105 90 105 225
Polygon -1 true false 135 75 165 180 150 240 255 240 285 225 255 150 210 105
Line -16777216 false 105 90 120 60
Line -16777216 false 120 45 120 240
Line -16777216 false 150 240 120 240
Line -16777216 false 135 75 120 60
Polygon -7500403 true true 120 60 75 45 120 30
Polygon -16777216 false false 105 90 75 120 30 195 15 240 105 225
Polygon -16777216 false false 135 75 165 180 150 240 255 240 285 225 255 150 210 105
Polygon -16777216 false false 0 239 60 299 225 299 240 284 270 269 270 254

sheep
false
15
Circle -1 true true 203 65 88
Circle -1 true true 70 65 162
Circle -1 true true 150 105 120
Polygon -7500403 true false 218 120 240 165 255 165 278 120
Circle -7500403 true false 214 72 67
Rectangle -1 true true 164 223 179 298
Polygon -1 true true 45 285 30 285 30 240 15 195 45 210
Circle -1 true true 3 83 150
Rectangle -1 true true 65 221 80 296
Polygon -1 true true 195 285 210 285 210 240 240 210 195 210
Polygon -7500403 true false 276 85 285 105 302 99 294 83
Polygon -7500403 true false 219 85 210 105 193 99 201 83

square
false
0
Rectangle -7500403 true true 30 30 270 270

square 2
false
0
Rectangle -7500403 true true 30 30 270 270
Rectangle -16777216 true false 60 60 240 240

star
false
0
Polygon -7500403 true true 151 1 185 108 298 108 207 175 242 282 151 216 59 282 94 175 3 108 116 108

target
false
0
Circle -7500403 true true 0 0 300
Circle -16777216 true false 30 30 240
Circle -7500403 true true 60 60 180
Circle -16777216 true false 90 90 120
Circle -7500403 true true 120 120 60

tree
false
0
Circle -7500403 true true 118 3 94
Rectangle -6459832 true false 120 195 180 300
Circle -7500403 true true 65 21 108
Circle -7500403 true true 116 41 127
Circle -7500403 true true 45 90 120
Circle -7500403 true true 104 74 152

triangle
false
0
Polygon -7500403 true true 150 30 15 255 285 255

triangle 2
false
0
Polygon -7500403 true true 150 30 15 255 285 255
Polygon -16777216 true false 151 99 225 223 75 224

truck
false
0
Rectangle -7500403 true true 4 45 195 187
Polygon -7500403 true true 296 193 296 150 259 134 244 104 208 104 207 194
Rectangle -1 true false 195 60 195 105
Polygon -16777216 true false 238 112 252 141 219 141 218 112
Circle -16777216 true false 234 174 42
Rectangle -7500403 true true 181 185 214 194
Circle -16777216 true false 144 174 42
Circle -16777216 true false 24 174 42
Circle -7500403 false true 24 174 42
Circle -7500403 false true 144 174 42
Circle -7500403 false true 234 174 42

turtle
true
0
Polygon -10899396 true false 215 204 240 233 246 254 228 266 215 252 193 210
Polygon -10899396 true false 195 90 225 75 245 75 260 89 269 108 261 124 240 105 225 105 210 105
Polygon -10899396 true false 105 90 75 75 55 75 40 89 31 108 39 124 60 105 75 105 90 105
Polygon -10899396 true false 132 85 134 64 107 51 108 17 150 2 192 18 192 52 169 65 172 87
Polygon -10899396 true false 85 204 60 233 54 254 72 266 85 252 107 210
Polygon -7500403 true true 119 75 179 75 209 101 224 135 220 225 175 261 128 261 81 224 74 135 88 99

wheel
false
0
Circle -7500403 true true 3 3 294
Circle -16777216 true false 30 30 240
Line -7500403 true 150 285 150 15
Line -7500403 true 15 150 285 150
Circle -7500403 true true 120 120 60
Line -7500403 true 216 40 79 269
Line -7500403 true 40 84 269 221
Line -7500403 true 40 216 269 79
Line -7500403 true 84 40 221 269

wolf
false
0
Polygon -16777216 true false 253 133 245 131 245 133
Polygon -7500403 true true 2 194 13 197 30 191 38 193 38 205 20 226 20 257 27 265 38 266 40 260 31 253 31 230 60 206 68 198 75 209 66 228 65 243 82 261 84 268 100 267 103 261 77 239 79 231 100 207 98 196 119 201 143 202 160 195 166 210 172 213 173 238 167 251 160 248 154 265 169 264 178 247 186 240 198 260 200 271 217 271 219 262 207 258 195 230 192 198 210 184 227 164 242 144 259 145 284 151 277 141 293 140 299 134 297 127 273 119 270 105
Polygon -7500403 true true -1 195 14 180 36 166 40 153 53 140 82 131 134 133 159 126 188 115 227 108 236 102 238 98 268 86 269 92 281 87 269 103 269 113

x
false
0
Polygon -7500403 true true 270 75 225 30 30 225 75 270
Polygon -7500403 true true 30 75 75 30 270 225 225 270
@#$#@#$#@
NetLogo 6.4.0
@#$#@#$#@
@#$#@#$#@
@#$#@#$#@
@#$#@#$#@
VIEW
338
71
883
502
0
0
0
1
1
1
1
1
0
1
1
1
0
319
0
119

MONITOR
7
64
135
113
Home port
NIL
3
1

MONITOR
7
10
261
59
Vessel information
NIL
3
1

MONITOR
8
126
241
175
user-id
NIL
3
1

MONITOR
11
192
167
241
Location
NIL
3
1

@#$#@#$#@
default
0.0
-0.2 0 0.0 1.0
0.0 1 1.0 0.0
0.2 0 0.0 1.0
link direction
true
0
Line -7500403 true 150 150 90 180
Line -7500403 true 150 150 210 180
@#$#@#$#@
0
@#$#@#$#@
