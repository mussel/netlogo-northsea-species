# SPDX-FileCopyrightText: 2023-2024 Helmholtz-Zentrum hereon GmbH
# SPDX-License-Identifier: CC0-1.0
# SPDX-FileContributor: Carsten Lemmen <carsten.lemmen@hereon.de>

FROM ubuntu:rolling AS ubuntulatex

LABEL description="Latex/pandoc/joss document generator based on Ubuntu"
LABEL author="Carsten Lemmen <carsten.lemmen@hereon.de>"
LABEL license="CC0-1.0"
LABEL copyright="2023-2024 Helmholtz-Zentrum hereon GmbH"

ENV LC_ALL=C.UTF-8 LANG=C.UTF-8

RUN apt-get update && apt-get install --no-install-recommends -y \
   make git \
   texlive-plain-generic texlive-fonts-recommended texlive-latex-extra \
   texlive-luatex texlive-fonts-extra fonts-hack fonts-open-sans \
   fonts-lmodern  citation-style-language-styles pandoc latexmk \
   openssl ca-certificates texlive-science wget dpkg

# We need to build pandoc with versions < 3.1.7 and > 3.1.7 for the
# change in handling CSLReferences
FROM ubuntulatex

ARG PANDOC_VERSION=3.1.6.2
ENV PANDOC_VERSION=${PANDOC_VERSION}
# Ubuntu distro contains a very outdated version of pandoc, so
# we pull the deb file and install it (it is a standalone binary)
SHELL ["/bin/bash", "-o", "pipefail", "-c"]
RUN arch=$(arch | sed s/aarch64/arm64/ | sed s/x86_64/amd64/) && \
    wget -q -O pandoc.deb --no-check-certificate  "https://github.com/jgm/pandoc/releases/download/${PANDOC_VERSION}/pandoc-${PANDOC_VERSION}-1-$arch.deb"
RUN dpkg -i pandoc.deb

# For this docker, emulate the behaviour of openjournals/inara, but
# create a bash entrypoint, not one running pandoc
RUN git clone --depth=1 https://github.com/openjournals/inara.git

ENV OPENJOURNALS_PATH=/usr/local/share/openjournals

RUN cp -r inara/resources ${OPENJOURNALS_PATH}
RUN cp -r inara/data ${OPENJOURNALS_PATH}/data
RUN cp inara/scripts/clean-metadata.lua ${OPENJOURNALS_PATH}
RUN cp inara/scripts/entrypoint.sh /usr/local/bin/joss
RUN ln -sf /usr/bin/pandoc /usr/local/bin/pandoc

ENV JOURNAL=joss

CMD [ "/bin/bash" ]
